# -*- coding: utf-8 -*-
import logging
import sys


def getLogger(name=__name__):
    # to run this test module with the output, you can eg. run:
    # pytest tests/test_mysql.py -rP
    # pytest tests/test_mysql.py -rx
    # -rP shows passed tests output ; -rx shows failed tests output
    # @see https://doc.pytest.org/en/latest/how-to/usage.html
    logging.basicConfig(stream=sys.stderr)
    logger = logging.getLogger(name)
    logger.setLevel(logging.DEBUG)
    return logger
