-- SQL dump, from a HERA database, using pyHeimdall
-- ------------------------------------------------------
-- pyHeimdall version	0.8.0

--
-- Table structure for table `pet`
--
DROP TABLE IF EXISTS `pet`;
CREATE TABLE `pet` (
`pet.name_attr` varchar(8) NOT NULL,
`pet.name_fr_attr` varchar(8) DEFAULT NULL,
`pet.owner_attr` varchar(6) DEFAULT NULL,
`pet.space_attr` varchar(7) DEFAULT NULL,
`pet.sex_attr` char(1) DEFAULT NULL,
`pet.birth_attr` date DEFAULT NULL,
`pet.death_attr` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
--
-- Dumping data for table `pet`
--
LOCK TABLE `pet` WRITE;
INSERT INTO `pet` VALUES ('Bowser','Bowser','Diane','dog','m','1979-08-31','1995-07-29'),('Buffy','Bouffi','Harold','dog','f','1989-05-13','2009-03-03'),('Chirpy','Cui-cui','Gwen','bird','f','1998-09-11',NULL),('Claws','Klaus','Gwen','cat','m','1994-03-17',NULL),('Fang',NULL,'Benny','dog','m','1990-08-27',NULL),('Fluffy','Touffu','Harold','cat','f','1993-02-04',NULL),('Puffball',NULL,'Diane','hamster','f','1999-03-30','2003-03-30'),('Slim',NULL,'Benny','snake','m','1996-04-29',NULL),('Whistler','Siffleur','Gwen','bird','n','1997-12-09',NULL);
UNLOCK TABLES;

--
-- Table structure for table `person`
--
DROP TABLE IF EXISTS `person`;
CREATE TABLE `person` (
`person.name_attr` varchar(6) NOT NULL,
`person.sex_attr` char(1) DEFAULT NULL,
`person.birth_attr` date DEFAULT NULL,
`person.death_attr` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
--
-- Dumping data for table `person`
--
LOCK TABLE `person` WRITE;
INSERT INTO `person` VALUES ('Benny','m','1944-02-29','2012-12-24'),('Diane','f','1970-04-04','2023-05-13'),('Gwen','f','1982-06-02',NULL),('Harold','m','1986-11-11',NULL);
UNLOCK TABLES;

--
-- Table structure for table `relationship`
--
DROP TABLE IF EXISTS `relationship`;
CREATE TABLE `relationship` (
`relationship.id_attr` int NOT NULL,
`relationship.owner_attr` varchar(6) DEFAULT NULL,
`relationship.pet_attr` varchar(8) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
--
-- Dumping data for table `relationship`
--
LOCK TABLE `relationship` WRITE;
INSERT INTO `relationship` VALUES ('1','Harold','Buffy'),('2','Harold','Fluffy'),('3','Gwen','Chirpy'),('4','Gwen','Claws'),('5','Gwen','Whistler'),('6','Diane','Bowser'),('7','Diane','Puffball'),('8','Benny','Fang'),('9','Benny','Slim');
UNLOCK TABLES;
