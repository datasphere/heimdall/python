# -*- coding: utf-8 -*-
import pytest
import os
from sys import version_info as _py
import heimdall
from ..stubs import *


EMPTY_DUMP = f'''-- SQL dump, from a HERA database, using pyHeimdall
-- ------------------------------------------------------
-- pyHeimdall version	{heimdall.__version__}
'''

EMPTY_ENTITY_NO_ITEMS_DUMP = f'''{EMPTY_DUMP}
--
-- Table structure for table `entity`
--
DROP TABLE IF EXISTS `entity`;
CREATE TABLE `entity` (
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
--
-- Dumping data for table `entity`
--
LOCK TABLE `entity` WRITE;
-- No data in table `entity`
UNLOCK TABLES;
'''

ONE_DUMP = f'''{EMPTY_DUMP}
--
-- Table structure for table `entity`
--
DROP TABLE IF EXISTS `entity`;
CREATE TABLE `entity` (
`uid` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
--
-- Dumping data for table `entity`
--
LOCK TABLE `entity` WRITE;
INSERT INTO `entity` VALUES ('1337');
UNLOCK TABLES;
'''

MINI_DUMP = f'''{EMPTY_DUMP}
--
-- Table structure for table `entity`
--
DROP TABLE IF EXISTS `entity`;
CREATE TABLE `entity` (
`uid` varchar(255) DEFAULT NULL,
`value` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
--
-- Dumping data for table `entity`
--
LOCK TABLE `entity` WRITE;
INSERT INTO `entity` VALUES ('101010','double');
UNLOCK TABLES;

--
-- Table structure for table `empty`
--
DROP TABLE IF EXISTS `empty`;
CREATE TABLE `empty` (
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;
--
-- Dumping data for table `empty`
--
LOCK TABLE `empty` WRITE;
-- No data in table `empty`
UNLOCK TABLES;
'''


@pytest.fixture
def folder(tmp_path):
    folder = tmp_path / 'test_mysql'
    folder.mkdir()
    yield folder


@pytest.mark.skipif(
        (_py.major, _py.minor) < (3, 8),
        reason="Requires python3.8 or higher"
    )
@pytest.mark.parametrize(('db', 'content'), [
    (like_rilly_empty_db, EMPTY_DUMP),
    (as_good_as_empty_db, EMPTY_DUMP),
    (orphan_attribute_db, EMPTY_ENTITY_NO_ITEMS_DUMP),
    (only_1_item_pea_db, ONE_DUMP),
    (mini_consistent_db, MINI_DUMP),
    ])
def test_create_database(folder, db, content):
    filename = str(folder / 'dump.sql')
    heimdall.createDatabase(db(), url=filename, format='sql:mysql')
    for root, dirs, files in os.walk(str(folder)):
        assert len(files) == 1
        path = os.path.join(root, files[0])
        assert path == filename
        with open(path, 'r') as f:
            _content = f.read()
            print(_content)
            assert _content == content


@pytest.mark.parametrize('db', [
    only_1_item_pea_db,
    mini_consistent_db,
    ])
def test_create_database_fails_if_repeatable_disallowed(folder, db):
    filename = str(folder / 'dump.sql')
    try:
        heimdall.createDatabase(db(), url=filename, format='sql:mysql',
                                allow_multivalues=False)  # <--- WE TEST THIS
        assert False
    except ValueError:
        assert True


@pytest.mark.skipif(
        (_py.major, _py.minor) < (3, 8),
        reason="Requires python3.8 or higher"
    )
def test_unavailable():
    from heimdall.connectors import mysql
    assert mysql.available is True


@pytest.mark.skipif(
        (_py.major, _py.minor) >= (3, 8),
        reason="Requires python3.7 or older"
    )
def test_unavailable():
    from heimdall.connectors import mysql
    assert mysql.available is False


@pytest.mark.skipif(
        (_py.major, _py.minor) >= (3, 8),
        reason="Requires python3.7 or older"
    )
def test_unsupported(folder):
    # GIVEN
    db = heimdall.util.tree.create_empty_tree()
    filename = str(folder / 'dump.sql')
    assert not os.path.isfile(filename)
    try:
        # WHEN
        heimdall.createDatabase(db, url=filename, format='sql:mysql')
        assert False
    except ModuleNotFoundError:
        # THEN
        assert not os.path.isfile(filename)


@pytest.mark.parametrize(('db', 'content'), [
    (mini_consistent_db, MINI_DUMP),
    ])
def test_create_database_does_not_overwrite(folder, db, content):
    folder = os.path.dirname(os.path.realpath(__file__))
    filename = os.path.join(folder, '..', 'examples', 'menagerie', 'hera.sql')
    try:
        heimdall.createDatabase(db(), url=filename, format='sql:mysql')
        assert False  # filename already exists, so we MUST break
    except ValueError as ex:
        assert filename in str(ex)
