# -*- coding: utf-8 -*-
import heimdall
import pytest
from ..stubs import *
from os.path import join, dirname, basename, realpath
from glob import glob
from importlib import import_module


@pytest.mark.parametrize('db_tested', [
    ({'url': MENAGERIE_BASIC_XML, 'format': 'hera:xml'}),
    ({'url': MENAGERIE_BASIC_YAML, 'format': 'hera:yaml'}),
    ({'url': MENAGERIE_BASIC_JSON, 'format': 'hera:json'}),
    ({'url': MENAGERIE_BASIC_CSV, 'format': 'csv'}),
    ({'url': REMOTE_MENAGERIE_XML, 'format': 'hera:xml'}),
    ({'url': REMOTE_MENAGERIE_YAML, 'format': 'hera:yaml'}),
    ({'url': REMOTE_MENAGERIE_JSON, 'format': 'hera:json'}),
    ])
@pytest.mark.parametrize('db_expected', [
    ({'url': MENAGERIE_BASIC_XML, 'format': 'hera:xml'}),
    ({'url': MENAGERIE_BASIC_YAML, 'format': 'hera:yaml'}),
    ({'url': MENAGERIE_BASIC_JSON, 'format': 'hera:json'}),
    ({'url': MENAGERIE_BASIC_CSV, 'format': 'csv'}),
    ({'url': REMOTE_MENAGERIE_XML, 'format': 'hera:xml'}),
    ({'url': REMOTE_MENAGERIE_YAML, 'format': 'hera:yaml'}),
    ({'url': REMOTE_MENAGERIE_JSON, 'format': 'hera:json'}),
    ])
def test_get_database(db_tested, db_expected):
    # WHEN
    db_real = heimdall.getDatabase(**db_tested)
    db_want = heimdall.getDatabase(**db_expected)
    # THEN
    properties_real = heimdall.getProperties(db_real)
    properties_want = heimdall.getProperties(db_want)
    assert len(properties_real) == len(properties_want)
    entities_real = heimdall.getEntities(db_real)
    entities_want = heimdall.getEntities(db_want)
    assert len(entities_real) == len(entities_want)
    for e in entities_real:
        eid = e.get('id')
        node = heimdall.getEntity(db_want, lambda n: n.get('id') == eid)
        assert node is not None
        attributes = heimdall.getAttributes(e)
        assert len(attributes) == len(heimdall.getAttributes(node))
        for a in attributes:
            aid = a.get('id', None)
            if aid is not None:
                n = heimdall.getAttribute(node, lambda n: n.get('id') == aid)
                assert n is not None
    items = heimdall.getItems(db_real)
    assert len(items) == len(heimdall.getItems(db_want))


@pytest.mark.parametrize(('content', 'format'), [
    ('<hera></hera>', 'hera:xml'),
    ('', 'hera:yaml'),
    ('{}', 'hera:yaml'),
    ('{}', 'hera:json'),
    ('', 'csv'),
    ])
def test_get_database_empty(tmp_path, content, format):
    # GIVEN
    path = tmp_path / 'empty.file'
    path.write_text(content)
    # WHEN
    tree = heimdall.getDatabase(url=str(path), format=format)
    # THEN
    assert len(heimdall.getProperties(tree)) == 0
    assert len(heimdall.getEntities(tree)) == 0
    assert len(heimdall.getItems(tree)) == 0


@pytest.mark.parametrize(('db', 'format', 'real_path', 'expected_path'), [
    # (menagerie_db, 'hera:xml', 'test.xml', MENAGERIE_BASIC_XML),
    (menagerie_db, 'hera:yaml', 'test.yaml', MENAGERIE_BASIC_YAML),
    (menagerie_db, 'hera:json', 'test.json', MENAGERIE_BASIC_JSON),
    ])
def test_serialize_to_file(tmp_path, db, format, real_path, expected_path):
    real_path = tmp_path / real_path
    # WHEN
    heimdall.createDatabase(db(), url=real_path, format=format)
    # THEN
    with open(real_path, 'r') as real:
        with open(expected_path, 'r') as expected:
            assert real.read().strip() == expected.read().strip()


@pytest.mark.parametrize(('db', 'format', 'expected_dir'), [
    (menagerie_db, 'csv', MENAGERIE_BASIC_CSV),
    ])
def test_serialize_to_dir(tmp_path, db, format, expected_dir):
    # WHEN
    heimdall.createDatabase(db(), url=tmp_path, format=format)
    # THEN
    real_dir = join(str(tmp_path), '*.csv')
    vs = dict()
    for expected_file in glob(expected_dir):
        key = basename(expected_file)
        vs[key] = [expected_file, ]
        for real_file in glob(real_dir):
            if real_file.endswith(key):
                vs[key].append(real_file)
    for key, paths in vs.items():
        expected_file = paths[0]
        real_file = paths[1]
        with open(real_file, 'r') as real:
            with open(expected_file, 'r') as expected:
                assert real.read().strip() == expected.read().strip()


@pytest.mark.parametrize(('content', 'format'), [
    ('<hera xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="https://gitlab.huma-num.fr/datasphere/hera/schema/schema.xsd" />', 'hera:xml'),  # nopep8: E501
    ('{}', 'hera:yaml'),
    ('{}', 'hera:json'),
    ])
def test_create_empty_database(tmp_path, content, format):
    # GIVEN
    tree = like_rilly_empty_db()
    path = tmp_path / 'empty.file'
    # WHEN
    tree = heimdall.createDatabase(tree, url=str(path), format=format)
    # THEN
    assert path.read_text().strip() == content


@pytest.mark.parametrize(('content', 'format'), [
    ('<hera xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="https://gitlab.huma-num.fr/datasphere/hera/schema/schema.xsd">\n  <properties />\n  <entities>\n    <entity id="entity" />\n  </entities>\n  <items>\n    <item eid="entity" />\n  </items>\n</hera>', 'hera:xml'),  # nopep8: E501
    ('entities:\n- id: entity\nitems:\n- eid: entity', 'hera:yaml'),
    ('{\n  "entities": [\n    {\n      "id": "entity"\n    }\n  ],\n  "items": [\n    {\n      "eid": "entity"\n    }\n  ]\n}', 'hera:json'),  # nopep8: E501
    ])
def test_create_nearly_empty_database(tmp_path, content, format):
    # GIVEN
    tree = only_1_item_entity()
    path = tmp_path / 'nirly_empty.file'
    # WHEN
    tree = heimdall.createDatabase(tree, url=str(path), format=format)
    # THEN
    assert path.read_text().strip() == content


@pytest.mark.parametrize(('connector'), [
    ('getDatabase'),
    ('createDatabase'),
    ])
@pytest.mark.parametrize(('submodule', 'options', 'flag', 'message'), [
    ('yaml', {'format': 'hera:yaml', 'url': 'unused', 'tree': None, }, '_installed', 'pyyaml'),  # nopep8: E501
    ])
def test_connector_breaks_if_dependency_not_installed(connector, submodule, options, flag, message):  # nopep8: E501
    # GIVEN
    module = import_module(f'heimdall.connectors.{submodule}')
    previous_value = getattr(module, flag)
    setattr(module, flag, False)
    try:
        # WHEN
        getattr(heimdall, connector)(**options)
        assert False  # never reached
    except ModuleNotFoundError as ex:
        # THEN
        assert message in str(ex)
    finally:
        setattr(module, flag, previous_value)
