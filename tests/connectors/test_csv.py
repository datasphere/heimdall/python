# -*- coding: utf-8 -*-
import pytest
import csv
import os
import heimdall
from ..stubs import *


@pytest.fixture
def folder(tmp_path):
    folder = tmp_path / 'test_csv'
    folder.mkdir()
    yield folder


@pytest.mark.parametrize(('db', 'content'), [
    (like_rilly_empty_db, None),
    (as_good_as_empty_db, None),
    (only_1_item_pea_db, {
        'entity': 'entity.uid\n1337\n'
    }),
    (mini_consistent_db, {
        'empty': '',
        'entity': 'entity.uid,entity.value\n101010,double\n',
    }),
    ])
def test_create_database(folder, db, content):
    heimdall.createDatabase(db(), url=folder, format='csv')
    for root, dirs, files in os.walk(str(folder)):
        assert len(files) == len(content or {})
        for file in files:
            eid = os.path.splitext(file)[0]
            assert eid in content.keys()
            path = os.path.join(root, file)
            with open(path, 'r') as f:
                assert f.read() == content[eid]


@pytest.mark.parametrize(('db'), [
    (like_rilly_empty_db),
    (as_good_as_empty_db),
    (only_1_item_pea_db),
    (mini_consistent_db),
    ])
def test_create_database_fails_if_path_is_file(folder, db):
    path = folder / 'already_a_file.wtf'
    path.write_text('whatever')
    assert os.path.isfile(path) is True
    try:
        heimdall.createDatabase(db(), url=path, format='csv')
        assert False
    except ValueError:
        assert True


@pytest.mark.parametrize(('db'), [
    (like_rilly_empty_db),
    (as_good_as_empty_db),
    (only_1_item_pea_db),
    (mini_consistent_db),
    ])
def test_create_database_fails_if_path_does_not_exist(folder, db):
    path = folder / 'subdir_does_not_exist'
    assert os.path.exists(path) is False
    try:
        heimdall.createDatabase(db(), url=path, format='csv')
        assert False
    except ValueError:
        assert True


def test_attr2col_attributes_with_no_pid():
    tree = heimdall.util.tree.create_empty_tree()
    eid = 'entity'
    entity = heimdall.createEntity(tree, eid)
    attributes = dict()
    for i in range(4):
        # GIVEN
        attribute = heimdall.elements.Attribute()
        entity.append(attribute)
        attributes[attribute] = f'{eid}.{i}'
    for attribute, expected_column_name in attributes.items():
        # WHEN
        column_name = heimdall.connectors.csv._attr2col(entity, attribute)
        # THEN
        assert column_name == expected_column_name


def test_attr2col_orphan_attribute():
    # GIVEN
    attribute = heimdall.elements.Attribute()
    # WHEN
    column_name = heimdall.connectors.csv._attr2col(None, attribute)
    # THEN
    assert column_name == '?'
