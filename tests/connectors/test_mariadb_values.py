# -*- coding: utf-8 -*-
import pytest
import heimdall
from heimdall.connectors.mysql import _dump_value
from ..stubs import *


@pytest.mark.parametrize(('pid', 'aid', 'values', 'expected'), [
    ('uid', 'uid', ['test'], "'test'"),
    ('uid',  None, ['test'], "'test'"),
    (None,  'uid', ['test'], "'test'"),
    (None,   None, ['test'], "NULL"),
    ('uid', 'uid', ['test1', 'test2', '3'], "'test1'|'test2'|'3'"),
    ('uid',  None, ['test1', 'test2', '3'], "'test1'|'test2'|'3'"),
    (None,  'uid', ['test1', 'test2', '3'], "'test1'|'test2'|'3'"),
    (None,   None, ['test1', 'test2', '3'], "NULL"),
    ])
def test_dump_value(pid, aid, values, expected):
    SEPARATOR = '|'
    db = single_attribute_db()
    e = heimdall.getEntities(db)[0]
    a = heimdall.getAttributes(e)[0]
    if pid is not None:
        a.attrib['pid'] = pid
    else:
        a.attrib.pop('pid', None)
    if aid is not None:
        a.attrib['id'] = aid
    else:
        a.attrib.pop('id', None)
    item = heimdall.createItem(db, eid=e.attrib['id'])
    # GIVEN
    for value in values:
        heimdall.createMetadata(item, value, pid=pid, aid=aid)
    # WHEN
    real = _dump_value(item, a, SEPARATOR)
    # THEN
    assert real == expected
