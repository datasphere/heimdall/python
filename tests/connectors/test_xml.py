# -*- coding: utf-8 -*-
import pytest
import heimdall
from ..stubs import *
import os


ROOT_ONLY_DUMP = """<hera xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="https://gitlab.huma-num.fr/datasphere/hera/schema/schema.xsd" />"""  # nopep8: E501

EMPTY_DUMP = """<hera xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="https://gitlab.huma-num.fr/datasphere/hera/schema/schema.xsd">
  <properties />
  <entities />
  <items />
</hera>"""  # nopep8: E501

ONE_DUMP = """<hera xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="https://gitlab.huma-num.fr/datasphere/hera/schema/schema.xsd">
  <properties>
    <property id="uid">
      <type>text</type>
      <name>Unique identifier</name>
    </property>
  </properties>
  <entities>
    <entity id="entity">
      <name>Some entity idk</name>
      <attribute pid="uid" min="0" />
    </entity>
  </entities>
  <items>
    <item eid="entity">
      <metadata pid="uid">1337</metadata>
      <metadata pid="name">shibboleet</metadata>
    </item>
  </items>
</hera>"""  # nopep8: E501

MINI_DUMP = """<hera xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="https://gitlab.huma-num.fr/datasphere/hera/schema/schema.xsd">
  <properties>
    <property id="uid">
      <type>text</type>
      <name>Unique identifier</name>
    </property>
    <property id="name">
      <type>text</type>
      <name>Human-readable identifier</name>
    </property>
    <property id="value">
      <type>text</type>
      <name>Some value idk lol</name>
    </property>
  </properties>
  <entities>
    <entity id="entity">
      <name>Some entity idk</name>
      <attribute pid="uid" min="0">
        <name>Unique identifier</name>
      </attribute>
      <attribute pid="value" min="0" />
    </entity>
    <entity id="empty" />
  </entities>
  <items>
    <item>
      <metadata pid="value">double</metadata>
    </item>
    <item eid="entity">
      <metadata pid="uid">101010</metadata>
      <metadata pid="value">double</metadata>
    </item>
    <item>
      <metadata pid="uid">1337</metadata>
      <metadata pid="name">shibboleet</metadata>
      <metadata pid="value">other</metadata>
    </item>
  </items>
</hera>"""  # nopep8: E501


@pytest.fixture
def folder(tmp_path):
    folder = tmp_path / 'test_xml'
    folder.mkdir()
    yield folder


@pytest.mark.parametrize(('db', 'content'), [
    (like_rilly_empty_db, ROOT_ONLY_DUMP),
    (as_good_as_empty_db, EMPTY_DUMP),
    (only_1_item_pea_db, ONE_DUMP),
    (mini_consistent_db, MINI_DUMP),
    ])
def test_create_database(folder, db, content):
    dst = folder / 'test.xml'
    heimdall.createDatabase(db(), url=dst, format='hera:xml')
    with open(dst, 'r') as f:
        assert f.read().strip() == content
