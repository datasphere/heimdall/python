# -*- coding: utf-8 -*-
import pytest
import heimdall
from heimdall.connectors.mysql import _get_type
from ..stubs import *


@pytest.mark.parametrize(('value', 'expected'), [
    ('datetime', 'date'),
    ('number', 'int'),
    ('text', 'varchar(255)'),
    ])
def test_get_type(value, expected):
    db = single_attribute_db()
    e = heimdall.getEntities(db)[0]
    a = heimdall.getAttributes(e)[0]
    # GIVEN
    a.type = value
    # WHEN
    t = _get_type(db, a)
    # THEN
    assert t == expected


@pytest.mark.parametrize(('value', 'expected'), [
    ('datetime', 'date'),
    ('number', 'int'),
    ('text', 'varchar(255)'),
    ])
def test_get_type_from_property(value, expected):
    db = single_attribute_db()
    e = heimdall.getEntities(db)[0]
    a = heimdall.getAttributes(e)[0]
    p = heimdall.getProperties(db)[0]
    p.type = value
    # GIVEN
    assert heimdall.util.tree.get_node(a, 'type') is None
    assert heimdall.util.tree.get_node(p, 'type') is not None
    # WHEN
    t = _get_type(db, a)
    # THEN
    assert t == expected


def test_get_type_no_type():
    db = single_attribute_db()
    e = heimdall.getEntities(db)[0]
    a = heimdall.getAttributes(e)[0]
    # GIVEN
    assert heimdall.util.tree.get_node(a, 'type') is None
    # WHEN
    t = _get_type(db, a)
    # THEN
    assert t == 'varchar(255)'


def test_get_type_no_type_even_in_property():
    db = single_attribute_db()
    e = heimdall.getEntities(db)[0]
    a = heimdall.getAttributes(e)[0]
    p = heimdall.getProperties(db)[0]
    p.type = None
    # GIVEN
    assert heimdall.util.tree.get_node(a, 'type') is None
    assert heimdall.util.tree.get_node(p, 'type') is None
    # WHEN
    t = _get_type(db, a)
    # THEN
    assert t == 'varchar(255)'


@pytest.mark.parametrize(('prefix', 'length', 'expected'), [
    ('len:', 1337, 'varchar(1337)'),
    ('len:', 255, 'varchar(255)'),
    ('len:',  42, 'varchar(42)'),
    ('len:',   1, 'char(1)'),
    ('len:',   0, 'char(1)'),
    ('len:',  -1, 'char(1)'),
    ('wtf?',  42, 'varchar(255)'),
    ])
def test_get_type_rule_length(prefix, length, expected):
    db = single_attribute_db()
    e = heimdall.getEntities(db)[0]
    a = heimdall.getAttributes(e)[0]
    p = heimdall.getProperties(db)[0]
    heimdall.util.tree.create_node(a, 'rule', f'{prefix}{length}')
    # GIVEN
    assert heimdall.util.tree.get_node(a, 'rule') is not None
    # WHEN
    t = _get_type(db, a)
    # THEN
    assert t == expected
