# -*- coding: utf-8 -*-
import pytest
import heimdall
from ..stubs import *
from heimdall.util.entities import (
    _get_metadata_info,
    _get_entity, _get_property,
    _get_container, _infer_type,
    _update_tree,
    ENTITIES, ATTRIBUTES, PROPERTIES,
    )
from xml.etree.ElementTree import ElementTree, XMLParser, fromstring
from copy import deepcopy


@pytest.mark.parametrize(('db', 'expected'), [
    (like_rilly_empty_db, 0),
    (as_good_as_empty_db, 0),
    (only_1_item_pea_db, 1),
    (mini_consistent_db, 3),
    # (menagerie_db, 14),
    ])
def test_update_entities(db, expected):
    # GIVEN
    before = db()
    after = heimdall.util.update_entities(before)
    # NOTE: remove all entities to make db non-relational
    #  (we don't bother to remove eid and aid from items)


METADATA_EMPTY = '''<metadata/>'''
METADATA_NONE = '''<metadata>V</metadata>'''
METADATA_PID = '''<metadata pid='P'>V</metadata>'''
METADATA_AID = '''<metadata aid='A'>V</metadata>'''
METADATA_BOTH = '''<metadata aid='A' pid='P'>V</metadata>'''
ATTR_PID = {'pid': 'P', }
ATTR_AID = {'id': 'A', }
ATTR_BOTH = {'id': 'A', 'pid': 'P', }
ATTR_APID = {'id': 'P', 'pid': 'P', }
ENTITY_EMPTY = {}
ENTITY_PID = {'attributes': [ATTR_PID, ]}
ENTITY_AID = {'attributes': [ATTR_AID, ]}
ENTITY_BOTH = {'attributes': [ATTR_BOTH, ]}
ITEM = f'''<item eid='E'>{METADATA_NONE}{METADATA_BOTH}{METADATA_AID}{METADATA_BOTH}{METADATA_PID}{METADATA_NONE}{METADATA_BOTH}{METADATA_NONE}</item>'''  # nopep8: E501
ITEM_NO_EID = f'''<item>{METADATA_PID}{METADATA_NONE}</item>'''
HERA = """<hera xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="https://gitlab.huma-num.fr/datasphere/hera/schema/schema.xsd">"""  # nopep8: E501
XML_ENTITY_PID = '''<entity id='E'><name>WPID</name><attribute pid='P'/></entity>'''  # nopep8: E501
XML_ENTITY_AID = '''<entity id='E'><name>WAID</name><attribute id='A'/></entity>'''  # nopep8: E501
XML_ENTITY_BOTH = '''<entity id='E'><name>WBOTH</name><attribute id='A' pid='P'/></entity>'''  # nopep8: E501
XML_PROPERTY_P = '''<property id='P'><name>PROPERTY</name></property>'''
XML_PROPERTY_X = '''<property id='X'><name>SUMTHING</name></property>'''
TREE_EMPTY = f"""{HERA}</hera>"""
TREE_ITEM_ONLY = f"""{HERA}
  <properties/>
  <entities/>
  <items>{ITEM}{ITEM_NO_EID}</items>
</hera>"""
TREE_ITEM_MIN0 = f"""{HERA}
  <properties/>
  <entities/>
  <items><item eid='E'/>{ITEM}{ITEM_NO_EID}</items>
</hera>"""
TREE_ENTITY_ONLY = f"""{HERA}
  <properties/>
  <entities>{XML_ENTITY_BOTH}</entities>
  <items/>
</hera>"""
TREE_PROPERTY_ONLY = f"""{HERA}
  <properties>{XML_PROPERTY_X}{XML_PROPERTY_P}</properties>
  <entities/>
  <items/>
</hera>"""
TREE_ENTITY_PID = f"""{HERA}
  <properties/>
  <entities>{XML_ENTITY_PID}</entities>
  <items>{ITEM}</items>
</hera>"""
TREE_ENTITY_AID = f"""{HERA}
  <properties/>
  <entities>{XML_ENTITY_AID}</entities>
  <items>{ITEM}</items>
</hera>"""
TREE_ENTITY_BOTH = f"""{HERA}
  <properties/>
  <entities>{XML_ENTITY_BOTH}</entities>
  <items>{ITEM}</items>
</hera>"""
TREE_PROPERTY_P = f"""{HERA}
  <properties>{XML_PROPERTY_P}</properties>
  <entities/>
  <items>{ITEM}</items>
</hera>"""
TREE_PROPERTY_X = f"""{HERA}
  <properties>{XML_PROPERTY_X}</properties>
  <entities/>
  <items>{ITEM}</items>
</hera>"""
TREE_PROPERTY_XP = f"""{HERA}
  <properties>{XML_PROPERTY_X}{XML_PROPERTY_P}</properties>
  <entities/>
  <items>{ITEM}</items>
</hera>"""


@pytest.mark.parametrize(('metadata', 'entity', 'expected'), [
    (METADATA_EMPTY, ENTITY_EMPTY, (None, None, None, ENTITY_EMPTY)),
    (METADATA_EMPTY, ENTITY_PID, (None, None, None, ENTITY_PID)),
    (METADATA_EMPTY, ENTITY_AID, (None, None, None, ENTITY_AID)),
    (METADATA_EMPTY, ENTITY_BOTH, (None, None, None, ENTITY_BOTH)),
    (METADATA_NONE, ENTITY_EMPTY, (None, None, None, ENTITY_EMPTY)),
    (METADATA_NONE, ENTITY_PID, (None, None, None, ENTITY_PID)),
    (METADATA_NONE, ENTITY_AID, (None, None, None, ENTITY_AID)),
    (METADATA_NONE, ENTITY_BOTH, (None, None, None, ENTITY_BOTH)),
    (METADATA_PID, ENTITY_EMPTY, ('P', 'P', ATTR_APID, {'attributes': [ATTR_APID, ]})),  # nopep8: E501
    (METADATA_PID, ENTITY_PID, ('P', 'P', ATTR_APID, {'attributes': [ATTR_APID, ]})),  # nopep8: E501
    (METADATA_PID, ENTITY_AID, ('P', 'P', ATTR_APID, {'attributes': [ATTR_AID, ATTR_APID, ]})),  # nopep8: E501
    (METADATA_PID, ENTITY_BOTH, ('P', 'A', ATTR_BOTH, ENTITY_BOTH)),
    (METADATA_AID, ENTITY_EMPTY, (None, 'A', ATTR_AID, ENTITY_AID)),
    (METADATA_AID, ENTITY_PID, (None, 'A', ATTR_AID, {'attributes': [ATTR_PID, ATTR_AID]})),  # nopep8: E501
    (METADATA_AID, ENTITY_AID, (None, 'A', ATTR_AID, ENTITY_AID)),
    (METADATA_AID, ENTITY_BOTH, ('P', 'A', ATTR_BOTH, ENTITY_BOTH)),
    (METADATA_BOTH, ENTITY_EMPTY, ('P', 'A', ATTR_BOTH, ENTITY_BOTH)),
    (METADATA_BOTH, ENTITY_PID, ('P', 'A', ATTR_BOTH, ENTITY_BOTH)),
    (METADATA_BOTH, ENTITY_AID, ('P', 'A', ATTR_BOTH, ENTITY_BOTH)),
    (METADATA_BOTH, ENTITY_BOTH, ('P', 'A', ATTR_BOTH, ENTITY_BOTH)),
    ])
def test_get_metadata_info(metadata, entity, expected):
    m = fromstring(metadata, XMLParser(target=heimdall.elements.Builder()))
    e = deepcopy(entity)  # clone to avoid tests side effects
    real = heimdall.util.entities._get_metadata_info(m, e)
    (pid, aid, attr) = real
    assert len(real) == 3
    assert pid == expected[0]
    assert aid == expected[1]
    assert attr == expected[2]
    assert e == expected[3]


@pytest.mark.parametrize(('xml', 'data', 'eid', 'expected'), [
    (TREE_ITEM_ONLY, {ENTITIES: {}}, '~', {'id': '~'}),
    (TREE_ITEM_ONLY, {ENTITIES: {}}, 'E', {'id': 'E'}),
    (TREE_ITEM_ONLY, {ENTITIES: {'E': {'id': 'E'}}}, 'E', {'id': 'E'}),
    (TREE_ENTITY_PID, {ENTITIES: {}}, '~', {'id': '~'}),
    (TREE_ENTITY_PID, {ENTITIES: {}}, 'E', {'id': 'E', 'name': "WPID", 'attributes': [{'pid': 'P', 'min': 0}]}),  # nopep8: E501
    (TREE_ENTITY_PID, {ENTITIES: {'E': {'id': 'E'}}}, 'E', {'id': 'E'}),
    (TREE_ENTITY_AID, {ENTITIES: {}}, '~', {'id': '~'}),
    (TREE_ENTITY_AID, {ENTITIES: {}}, 'E', {'id': 'E', 'name': "WAID", 'attributes': [{'id': 'A', 'pid': None, 'min': 0}]}),  # nopep8: E501
    (TREE_ENTITY_AID, {ENTITIES: {'E': {'id': 'E'}}}, 'E', {'id': 'E'}),
    (TREE_ENTITY_BOTH, {ENTITIES: {}}, '~', {'id': '~'}),
    (TREE_ENTITY_BOTH, {ENTITIES: {}}, 'E', {'id': 'E', 'name': "WBOTH", 'attributes': [{'id': 'A', 'pid': 'P', 'min': 0}]}),  # nopep8: E501
    (TREE_ENTITY_BOTH, {ENTITIES: {'E': {'id': 'E'}}}, 'E', {'id': 'E'}),
    ])
def test_get_entity(xml, data, eid, expected):
    tree = fromstring(xml, XMLParser(target=heimdall.elements.Builder()))
    entity = _get_entity(tree, data, eid)
    assert entity == expected
    assert data[ENTITIES][eid] == expected


@pytest.mark.parametrize(('xml', 'data', 'pid', 'aid', 'expected'), [
    (TREE_ITEM_ONLY, {PROPERTIES: {}}, None, None, None),
    (TREE_ITEM_ONLY, {PROPERTIES: {}}, '~', None, {'id': '~'}),
    (TREE_ITEM_ONLY, {PROPERTIES: {}}, None, '~', {'id': '~'}),
    (TREE_ITEM_ONLY, {PROPERTIES: {}}, 'P', '~', {'id': 'P'}),
    (TREE_ITEM_ONLY, {PROPERTIES: {}}, 'P', None, {'id': 'P'}),
    (TREE_ITEM_ONLY, {PROPERTIES: {}}, None, 'A', {'id': 'A'}),
    (TREE_ITEM_ONLY, {PROPERTIES: {'P': {'id': 'P', 'name': "TEST"}}}, 'P', '~', {'id': 'P', 'name': "TEST"}),  # nopep8: E501
    (TREE_ITEM_ONLY, {PROPERTIES: {'P': {'id': 'P', 'name': "TEST"}}}, 'P', None, {'id': 'P', 'name': "TEST"}),  # nopep8: E501
    (TREE_ITEM_ONLY, {PROPERTIES: {'P': {'id': 'P', 'name': "TEST"}}}, None, 'A', {'id': 'A'}),  # nopep8: E501
    (TREE_PROPERTY_P, {PROPERTIES: {}}, None, None, None),
    (TREE_PROPERTY_P, {PROPERTIES: {}}, '~', None, {'id': '~'}),
    (TREE_PROPERTY_P, {PROPERTIES: {}}, None, '~', {'id': '~'}),
    (TREE_PROPERTY_P, {PROPERTIES: {}}, 'P', '~', {'id': 'P', 'name': "PROPERTY"}),  # nopep8: E501
    (TREE_PROPERTY_P, {PROPERTIES: {}}, 'P', None, {'id': 'P', 'name': "PROPERTY"}),  # nopep8: E501
    (TREE_PROPERTY_P, {PROPERTIES: {}}, None, 'A', {'id': 'A'}),
    (TREE_PROPERTY_P, {PROPERTIES: {'X': {'id': 'X'}}}, 'P', '~', {'id': 'P', 'name': "PROPERTY"}),  # nopep8: E501
    (TREE_PROPERTY_P, {PROPERTIES: {'X': {'id': 'X'}}}, 'P', None, {'id': 'P', 'name': "PROPERTY"}),  # nopep8: E501
    (TREE_PROPERTY_P, {PROPERTIES: {'X': {'id': 'X'}}}, None, 'A', {'id': 'A'}),  # nopep8: E501
    (TREE_PROPERTY_P, {PROPERTIES: {'P': {'id': 'P', 'name': "TEST"}}}, 'P', '~', {'id': 'P', 'name': "TEST"}),  # nopep8: E501
    (TREE_PROPERTY_P, {PROPERTIES: {'P': {'id': 'P', 'name': "TEST"}}}, 'P', None, {'id': 'P', 'name': "TEST"}),  # nopep8: E501
    (TREE_PROPERTY_P, {PROPERTIES: {'P': {'id': 'P', 'name': "TEST"}}}, None, 'A', {'id': 'A'}),  # nopep8: E501
    (TREE_PROPERTY_X, {PROPERTIES: {}}, None, None, None),
    (TREE_PROPERTY_X, {PROPERTIES: {}}, '~', None, {'id': '~'}),
    (TREE_PROPERTY_X, {PROPERTIES: {}}, None, '~', {'id': '~'}),
    (TREE_PROPERTY_X, {PROPERTIES: {}}, 'P', '~', {'id': 'P'}),
    (TREE_PROPERTY_X, {PROPERTIES: {}}, 'P', None, {'id': 'P'}),
    (TREE_PROPERTY_X, {PROPERTIES: {}}, None, 'A', {'id': 'A'}),
    (TREE_PROPERTY_X, {PROPERTIES: {'X': {'id': 'X'}}}, 'P', '~', {'id': 'P'}),
    (TREE_PROPERTY_X, {PROPERTIES: {'X': {'id': 'X'}}}, 'P', None, {'id': 'P'}),  # nopep8: E501
    (TREE_PROPERTY_X, {PROPERTIES: {'X': {'id': 'X'}}}, None, 'A', {'id': 'A'}),  # nopep8: E501
    (TREE_PROPERTY_X, {PROPERTIES: {'P': {'id': 'P', 'name': "TEST"}}}, 'P', '~', {'id': 'P', 'name': "TEST"}),  # nopep8: E501
    (TREE_PROPERTY_X, {PROPERTIES: {'P': {'id': 'P', 'name': "TEST"}}}, 'P', None, {'id': 'P', 'name': "TEST"}),  # nopep8: E501
    (TREE_PROPERTY_X, {PROPERTIES: {'P': {'id': 'P', 'name': "TEST"}}}, None, 'A', {'id': 'A'}),  # nopep8: E501
    (TREE_PROPERTY_XP, {PROPERTIES: {}}, None, None, None),
    (TREE_PROPERTY_XP, {PROPERTIES: {}}, '~', None, {'id': '~'}),
    (TREE_PROPERTY_XP, {PROPERTIES: {}}, None, '~', {'id': '~'}),
    (TREE_PROPERTY_XP, {PROPERTIES: {}}, 'P', '~', {'id': 'P', 'name': "PROPERTY"}),  # nopep8: E501
    (TREE_PROPERTY_XP, {PROPERTIES: {}}, 'P', None, {'id': 'P', 'name': "PROPERTY"}),  # nopep8: E501
    (TREE_PROPERTY_XP, {PROPERTIES: {}}, None, 'A', {'id': 'A'}),
    (TREE_PROPERTY_XP, {PROPERTIES: {'X': {'id': 'X'}}}, 'P', '~', {'id': 'P', 'name': "PROPERTY"}),  # nopep8: E501
    (TREE_PROPERTY_XP, {PROPERTIES: {'X': {'id': 'X'}}}, 'P', None, {'id': 'P', 'name': "PROPERTY"}),  # nopep8: E501
    (TREE_PROPERTY_XP, {PROPERTIES: {'X': {'id': 'X'}}}, None, 'A', {'id': 'A'}),  # nopep8: E501
    (TREE_PROPERTY_XP, {PROPERTIES: {'P': {'id': 'P', 'name': "TEST"}}}, 'P', '~', {'id': 'P', 'name': "TEST"}),  # nopep8: E501
    (TREE_PROPERTY_XP, {PROPERTIES: {'P': {'id': 'P', 'name': "TEST"}}}, 'P', None, {'id': 'P', 'name': "TEST"}),  # nopep8: E501
    (TREE_PROPERTY_XP, {PROPERTIES: {'P': {'id': 'P', 'name': "TEST"}}}, None, 'A', {'id': 'A'}),  # nopep8: E501
    ])
def test_get_property(xml, data, pid, aid, expected):
    tree = ElementTree(fromstring(xml))
    p = _get_property(tree, data, pid, aid)
    assert p == expected
    try:
        assert data[PROPERTIES][pid] == expected
    except KeyError:
        assert pid is None


@pytest.mark.parametrize(('xml', 'tag', 'before', 'after'), [
    (TREE_EMPTY, 'properties', 0, 1),
    (TREE_EMPTY, 'entities', 0, 1),
    (TREE_EMPTY, 'items', 0, 1),
    (TREE_EMPTY, 'whatever', 0, 1),
    (TREE_ITEM_ONLY, 'properties', 3, 3),
    (TREE_ITEM_ONLY, 'entities', 3, 3),
    (TREE_ITEM_ONLY, 'items', 3, 3),
    (TREE_ITEM_ONLY, 'whatever', 3, 4),
    ])
def test_get_container(xml, tag, before, after):
    root = fromstring(xml, XMLParser(target=heimdall.elements.Builder()))
    assert len(root.children) == before
    assert _get_container(root, tag) is not None
    assert len(root.children) == after


@pytest.mark.parametrize(('given', 'expected'), [
    (0, 'number'),
    (42, 'number'),
    (3.14, 'number'),
    ('polop', 'text'),
    ('حيوان أليف', 'text'),
    ('宠儿', 'text'),
    ('🐶🐱🐭🐹🐰🦜🐠🐢', 'text'),
    # ('24.12.2011', 'datetime'),
    ])
def test_infer_type(given, expected):
    assert _infer_type(given) == expected


@pytest.mark.parametrize(('xml', 'delete_orphans', 'expected'), [
    (TREE_EMPTY, False, {
        'properties': [],
        'entities': [],
        'items': [],
        }),
    (TREE_PROPERTY_ONLY, False, {
        'properties': [{'id': 'X'}, {'id': 'P'}],
        'entities': [],
        'items': [],
        }),
    (TREE_ENTITY_ONLY, False, {
        'properties': [],
        'entities': [{'id': 'E', 'attributes': [{'id': 'A', 'pid': 'P'}]}],
        'items': [],
        }),
    (TREE_ITEM_ONLY, False, {
        'properties': [{'id': 'P'}],
        'entities': [{'id': 'E', 'attributes': [{'id': 'A', 'pid': 'P'}]}],
        'items': [
            {'eid': 'E', 'metadata': [
                {'value': "V"},
                {'aid': 'A', 'pid': 'P', 'value': "V"},
                {'aid': 'A', 'value': "V"},
                {'aid': 'A', 'pid': 'P', 'value': "V"},
                {'pid': 'P', 'value': "V"},
                {'value': "V"},
                {'aid': 'A', 'pid': 'P', 'value': "V"},
                {'value': "V"},
                ]},
            {'metadata': [
                {'pid': 'P', 'value': "V"},
                {'value': "V"},
                ]},
            ],
        }),
    (TREE_ITEM_MIN0, False, {
        'properties': [{'id': 'P'}],
        'entities': [{'id': 'E', 'attributes': [{'id': 'A', 'pid': 'P'}]}],
        'items': [
            {'eid': 'E', 'metadata': []},
            {'eid': 'E', 'metadata': [
                {'value': "V"},
                {'aid': 'A', 'pid': 'P', 'value': "V"},
                {'aid': 'A', 'value': "V"},
                {'aid': 'A', 'pid': 'P', 'value': "V"},
                {'pid': 'P', 'value': "V"},
                {'value': "V"},
                {'aid': 'A', 'pid': 'P', 'value': "V"},
                {'value': "V"},
                ]},
            {'metadata': [
                {'pid': 'P', 'value': "V"},
                {'value': "V"},
                ]},
            ],
        }),
    (TREE_EMPTY, True, {
        'properties': [],
        'entities': [],
        'items': [],
        }),
    (TREE_PROPERTY_ONLY, True, {
        'properties': [{'id': 'X'}, {'id': 'P'}],
        'entities': [],
        'items': [],
        }),
    (TREE_ENTITY_ONLY, True, {
        'properties': [],
        'entities': [{'id': 'E', 'attributes': [{'id': 'A', 'pid': 'P'}]}],
        'items': [],
        }),
    (TREE_ITEM_ONLY, True, {
        'properties': [{'id': 'P'}],
        'entities': [{'id': 'E', 'attributes': [{'id': 'A', 'pid': 'P'}]}],
        'items': [
            {'eid': 'E', 'metadata': [
                # NOTE: orphan (that was here) must be deleted
                {'aid': 'A', 'pid': 'P', 'value': "V"},
                {'aid': 'A', 'value': "V"},
                {'aid': 'A', 'pid': 'P', 'value': "V"},
                {'pid': 'P', 'value': "V"},
                # NOTE: orphan (that was here) must be deleted
                {'aid': 'A', 'pid': 'P', 'value': "V"},
                # NOTE: orphan (that was here) must be deleted
                ]},
            {'metadata': [
                {'pid': 'P', 'value': "V"},
                {'value': "V"},
                ]},
            ],
        }),
    (TREE_ITEM_MIN0, True, {
        'properties': [{'id': 'P'}],
        'entities': [{'id': 'E', 'attributes': [{'id': 'A', 'pid': 'P'}]}],
        'items': [
            {'eid': 'E', 'metadata': []},
            {'eid': 'E', 'metadata': [
                # NOTE: orphan (that was here) must be deleted
                {'aid': 'A', 'pid': 'P', 'value': "V"},
                {'aid': 'A', 'value': "V"},
                {'aid': 'A', 'pid': 'P', 'value': "V"},
                {'pid': 'P', 'value': "V"},
                # NOTE: orphan (that was here) must be deleted
                {'aid': 'A', 'pid': 'P', 'value': "V"},
                # NOTE: orphan (that was here) must be deleted
                ]},
            {'metadata': [
                {'pid': 'P', 'value': "V"},
                {'value': "V"},
                ]},
            ],
        }),
    ])
def test_update_entities(xml, delete_orphans, expected):
    # GIVEN
    before = fromstring(xml, XMLParser(target=heimdall.elements.Builder()))
    # WHEN
    after = heimdall.util.update_entities(before, delete_orphans)
    # THEN
    assert after == before  # changes were performed "in place"
    want = expected['properties']
    real = heimdall.getProperties(after)
    assert len(real) == len(want)
    for property_, data in zip(real, want):
        assert property_.get('id') == data['id']
    want = expected['entities']
    real = heimdall.getEntities(after)
    assert len(real) == len(want)
    for entity, data in zip(real, want):
        assert entity.get('id') == data['id']
        attributes = heimdall.getAttributes(entity)
        assert len(attributes) == len(data['attributes'])
        for a, d in zip(attributes, data['attributes']):
            aid = a.get('id', None)
            try:
                assert aid == d['id']
            except KeyError:
                assert aid is None
            pid = a.get('pid', None)
            try:
                assert pid == d['pid']
            except KeyError:
                assert pid is None
    want = expected['items']
    real = heimdall.getItems(after)
    assert len(real) == len(want)
    for item, data in zip(real, want):
        assert item.get('eid') == data.get('eid', None)
        metadata = heimdall.getMetadata(item)
        assert len(metadata) == len(data['metadata'])
        for m, d in zip(metadata, data['metadata']):
            aid = m.get('aid', None)
            try:
                assert aid == d['aid']
            except KeyError:
                assert aid is None
            pid = m.get('pid', None)
            try:
                assert pid == d['pid']
            except KeyError:
                assert pid is None
            assert m.text == d['value']


@pytest.mark.parametrize(('db', 'getter', 'before', 'data', 'after'), [
    (mini_consistent_db, heimdall.getProperty,
     {PROPERTIES: {'uid': {'name': {None: ["Unique identifier"]}}, }, },
     {PROPERTIES: {'uid': {'name': "Test"}, }, ENTITIES: {}, },
     {PROPERTIES: {'uid': {'name': {None: ["Test"]}}, }, },
     ),
    (mini_consistent_db, heimdall.getProperty,
     {PROPERTIES: {'uid': {'name': {None: ["Unique identifier"]}}, }, },
     {PROPERTIES: {'test': {'id': 'test', 'name': "bar"}, }, ENTITIES: {}, },
     {PROPERTIES: {
         'uid': {'name': {None: ["Unique identifier"]}},
         'test': {'id': 'test', 'name': {None: ["bar"]}},
         },
      },
     ),
    (mini_consistent_db, heimdall.getEntity,
     {ENTITIES: {'empty': {'id': 'empty'}, }, },
     {ENTITIES: {'test': {'id': 'test', ATTRIBUTES: {}}, }, PROPERTIES: {}},
     {ENTITIES: {'empty': {'id': 'empty'}, 'test': {'id': 'test', }}, },
     ),
    (mini_consistent_db, heimdall.getEntity,
     {ENTITIES: {'entity': {'id': 'entity'}, }, },
     {ENTITIES: {'entity': {'id': 'entity', ATTRIBUTES: {}}, }, PROPERTIES: {}},  # nopep8: E501
     {ENTITIES: {'entity': {'id': 'entity'}, }, },
     ),
    (mini_consistent_db, heimdall.getEntity,
     {ENTITIES: {'entity': {'id': 'entity'}, }, },
     {ENTITIES: {'entity': {'id': 'entity', ATTRIBUTES: [{'id': 'foo', 'pid': 'uid', 'name': "test"}]}, }, PROPERTIES: {}},  # nopep8: E501
     {ENTITIES: {'entity': {'id': 'entity'}, }, },
     ),
    ])
def test_update_tree(db, getter, before, data, after):
    def _assert(tree, expected):
        for container, children in expected.items():
            for id, expected in children.items():
                node = getter(tree, lambda x: x.get('id') == id)
                for k, v in expected.items():
                    if k in ['id', ]:
                        assert node.get(k) == v
                    else:
                        assert getattr(node, k) == v

    tree = db()
    _assert(tree, before)
    _update_tree(tree, data)
    _assert(tree, after)
