# -*- coding: utf-8 -*-
import heimdall
from os.path import join, dirname, realpath


def like_rilly_empty_db():
    root = heimdall.util.tree.create_empty_tree()
    for container in [c for c in root.iter() if c is not root]:
        root.remove(container)
    return root


def as_good_as_empty_db():
    return heimdall.util.tree.create_empty_tree()


def only_1_item_entity():
    tree = heimdall.util.tree.create_empty_tree()
    e = heimdall.createEntity(tree, 'entity')
    heimdall.createItem(tree, eid='entity')
    return tree


def only_1_item_pea_db():
    tree = heimdall.util.tree.create_empty_tree()
    heimdall.createProperty(tree, 'uid', name="Unique identifier")
    e = heimdall.createEntity(tree, 'entity', name="Some entity idk")
    heimdall.createAttribute(e, pid='uid')
    heimdall.createItem(tree, eid='entity', uid='1337', name='shibboleet')
    return tree


def mini_consistent_db():
    tree = heimdall.util.tree.create_empty_tree()

    heimdall.createProperty(tree, 'uid', name="Unique identifier")
    heimdall.createProperty(tree, 'name', name="Human-readable identifier")
    heimdall.createProperty(tree, 'value', name="Some value idk lol")

    e = heimdall.createEntity(tree, 'entity', name="Some entity idk")
    heimdall.createAttribute(e, pid='uid', name="Unique identifier")
    heimdall.createAttribute(e, pid='value')
    heimdall.createEntity(tree, 'empty')

    heimdall.createItem(tree, value='double')
    heimdall.createItem(tree, eid='entity', uid='101010', value='double')
    heimdall.createItem(tree, uid='1337', name='shibboleet', value='other')
    return tree


def single_attribute_db():
    tree = heimdall.util.tree.create_empty_tree()
    heimdall.createProperty(tree, 'uid')
    e = heimdall.createEntity(tree, 'entity')
    a = heimdall.createAttribute(e, pid='uid', name={'en': "Orphan"})
    return tree


def orphan_attribute_db():
    tree = single_attribute_db()
    a = heimdall.getAttributes(heimdall.getEntities(tree)[0])[0]
    a.attrib.pop('pid')
    return tree


EXAMPLES_DIR = join(dirname(realpath(__file__)), 'examples')
MENAGERIE_BASIC_XML = join(EXAMPLES_DIR, 'menagerie', 'hera.xml')
MENAGERIE_BASIC_YAML = join(EXAMPLES_DIR, 'menagerie', 'hera.yaml')
MENAGERIE_BASIC_JSON = join(EXAMPLES_DIR, 'menagerie', 'hera.json')
MENAGERIE_BASIC_CSV = join(EXAMPLES_DIR, 'menagerie', '*.csv')
MENAGERIE_BASIC_SQL = join(EXAMPLES_DIR, 'menagerie', 'hera.sql')

REMOTE_EXAMPLES = 'https://gitlab.huma-num.fr/datasphere/heimdall/python/raw/main/tests/examples'  # nopep8: E501
REMOTE_MENAGERIE_XML = join(REMOTE_EXAMPLES, 'menagerie', 'hera.xml')
REMOTE_MENAGERIE_YAML = join(REMOTE_EXAMPLES, 'menagerie', 'hera.yaml')
REMOTE_MENAGERIE_JSON = join(REMOTE_EXAMPLES, 'menagerie', 'hera.json')


def menagerie_db():
    path = join(dirname(realpath(__file__)), 'examples', 'menagerie')
    return heimdall.getDatabase(
            format='hera:xml',
            url=MENAGERIE_BASIC_XML,
            )
