# -*- coding: utf-8 -*-
import pytest
import heimdall


@pytest.mark.parametrize('decorator', [
    'get_database',
    'create_database',
    ])
def test_decorator_exists(decorator):
    format = 'test:format'
    decorator = getattr(heimdall.decorators, decorator)
    assert decorator is not None


@pytest.mark.parametrize(('decorator', 'formats'), [
    ('get_database', ['csv', 'hera:xml', 'hera:json', 'hera:yaml', ]),
    ('create_database', [
      'csv', 'hera:xml', 'hera:json', 'hera:yaml',
      'sql:mariadb', 'sql:mysql',
      ]),
    ])
def test_format_known_for_decorator(decorator, formats):
    connectors = heimdall.heimdall.CONNECTORS[decorator]
    for format in formats:
        assert format in connectors


@pytest.mark.parametrize('decorator', [
    'get_database',
    'create_database',
    ])
def test_decorator_is_function(decorator):
    decorator = getattr(heimdall.decorators, decorator)
    f = decorator('test:format')
    assert f is not None
    assert callable(f)
    assert hasattr(f, '__call__')


@pytest.mark.parametrize('decorator', [
    'get_database',
    'create_database',
    ])
def test_decorator_adds_format(decorator):
    def stub():
        pass

    FORMAT = 'test:format'
    formats = heimdall.heimdall.CONNECTORS[decorator]
    assert FORMAT not in formats.keys()
    length_before = len(formats.keys())
    decorator = getattr(heimdall.decorators, decorator)
    decorator(FORMAT)(stub)
    assert FORMAT in formats.keys()
    assert len(formats.keys()) == length_before+1


@pytest.mark.parametrize('decorator', [
    'get_database',
    'create_database',
    ])
def test_decorator_calls_function(decorator):
    def stub(x):
        return f'{x}, bro'

    decorator = getattr(heimdall.decorators, decorator)
    assert decorator('test:format')(stub)(42) == '42, bro'
