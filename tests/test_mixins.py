# -*- coding: utf-8 -*-
import pytest
from heimdall.elements import *


@pytest.mark.parametrize('cls', [
    Item, Metadata, Property, Entity, Attribute,
    ])
def test_element_tag(cls):
    assert cls().tag == cls.__name__.lower()


@pytest.mark.parametrize(('cls', 'expected'), [
    (Root, 'hera'), (Item, 'item'), (Metadata, 'metadata'),
    (Property, 'property'), (Entity, 'entity'), (Attribute, 'attribute'),
    ])
def test_element_tag_hard_coded(cls, expected):
    # NOTE: /!\ IMPORTANT /!\
    # okay, this test case is arguably the same as the `test_element_tag`;
    # however, as changing any of this classes' TAG attributes will prolly
    # break EVERY third party software using the format, I hard-coded expected
    # TAG attributes heren so I made you think TWICE or THRICE before doing it.
    # In other words: DON'T CHANGE TAGS, unless you reeeeeaaaaally assume the
    #                 consequences (and this should be a major version update)
    assert cls().tag == expected


@pytest.mark.parametrize('cls', [
    Root,
    ])
@pytest.mark.parametrize('container', [
    '', 'items', 'entities', 'properties', 'whatever', None,
    "حيوان أليف", "寵物", "🐶🐱🐭🐹🐰🦜🐠🐢",
    ])
def test_get_container_empty(cls, container):
    assert cls().get_container(container) is None


@pytest.mark.parametrize('cls', [
    Entity,
    ])
def test_get_attributes_empty(cls):
    assert cls().attributes is not None
    assert len(cls().attributes) == 0


@pytest.mark.parametrize('cls', [
    Root, Item, Metadata, Property, Entity, Attribute,
    ])
def test_element_text_is_none_if_not_provided(cls):
    assert cls().text is None


@pytest.mark.parametrize('cls', [
    Root, Item, Metadata, Property, Entity, Attribute,
    ])
@pytest.mark.parametrize('text', [
    "", "empty", "text that\ncontains \nnewlines!",
    "حيوان أليف", "寵物", "🐶🐱🐭🐹🐰🦜🐠🐢",
    ])
def test_element_text(cls, text):
    node = cls()
    node.text = text
    assert node.text == text


@pytest.mark.parametrize('cls', [
    Root, Item, Metadata, Property, Entity, Attribute,
    ])
def test_element_text_cannot_be_none_at_creation(cls):
    try:
        cls(None)
        assert False
    except TypeError:
        assert True


@pytest.mark.parametrize('cls', [
    Root, Item, Metadata, Property, Entity, Attribute,
    ])
def test_element_text_cannot_be_multiple_words(cls):
    try:
        cls("multiple", "words")
        assert False
    except TypeError:
        assert True


@pytest.mark.parametrize('cls', [
    Root, Item, Metadata, Property, Entity, Attribute,
    ])
@pytest.mark.parametrize('params', [
    {},
    {'spam': 'eggs'},
    {'empty': ''},
    {
      'empty': '', 'multi': 'text that\ncontains \nnewlines!',
      'arabic': 'حيوان أليف', 'chinese': '寵物', 'floofs': '🐶🐱🐭🐹🐰🦜🐠🐢',
    }
    ])
def test_element_attributes_at_creation(cls, params):
    element = cls(**params)
    for key, value in params.items():
        assert element.get(key) == value  # this works
        assert not hasattr(element, key)
        try:
            getattr(element, key) == value  # this breaks
            assert False
        except AttributeError as ex:
            assert key in str(ex)


@pytest.mark.parametrize('cls', [
    Root, Item, Metadata, Property, Entity, Attribute,
    ])
def test_element_attribute_cannot_be_none(cls):
    try:
        cls(**{None: 'whatever'})
        assert False
    except TypeError as ex:
        assert True


@pytest.mark.parametrize('cls', [
    Root, Item, Metadata, Property, Entity, Attribute,
    ])
def test_element_attribute_value_can_be_none(cls):
    element = cls(**{'value': None})
    assert element.get('value') is None


@pytest.mark.parametrize('cls', [
    Root, Item, Metadata, Property, Entity, Attribute,
    ])
def test_custom_element_hierarchy_is_as_expected_and_base_is_mixable(cls):
    bases = cls.__bases__
    # NOTE: __bases__ only goes up one level; this is enough as, at the time
    # I'm writing this test, all of our custom elements inherit directly from
    # `xml.etree.ElementTree.Element`; if this changes in the future,
    # the way "base" class is retrieved in this test should be updated
    base = bases[-1]
    # NOTE: with bases[-1], we take the last base class that was declared in
    # the custom element class profile; this is in accordance with the fact
    # that in Python, methods are resolved from left to right, and thus the
    # best practice is to declare the mixins first, then the "base" class last.
    # Thus, make sure that if you add new custom elements, their inheritance
    # is properly declared, with "base" class last, to the right.
    # @see https://www.ianlewis.org/en/mixins-and-python
    assert base.__name__ == 'Element'
    attrs = dir(base)
    assert '__init__' in attrs
    assert 'id' not in attrs
    # Documentable
    assert 'name' not in attrs
    assert 'description' not in attrs
    assert 'uri' not in attrs
    # Typable
    assert 'type' not in attrs
    # Repeatable
    assert 'min' not in attrs
    assert 'max' not in attrs


@pytest.mark.parametrize('cls', [
    Property, Entity, Attribute,
    ])
@pytest.mark.parametrize('key', [
    'name', 'description',
    ])
@pytest.mark.parametrize('value', [
    "test", "", None,
    ["test", ], ["test1", "test2", ], ["", ], [None, ],
    {'fr': ["Nom", ], 'en': ["Name", ], },
    {'en': ["Name", ], 'fr': ["Nom", "Titre", "Appelation", ], },
    {'en': ["Name", "Title", ], 'fr': [None, ], },
    {'en': ["Name", "Title", ], 'fr': ["Nom", "Titre", "Appelation", ], },
    ])
def test_element_documentable_mixin_i18n(cls, key, value):
    # GIVEN
    node = cls()
    assert len(getattr(node, key)) == 0
    # WHEN
    setattr(node, key, value)
    # THEN
    real = getattr(node, key)
    if type(value) is str or type(value) is type(None):
        assert len(real[None]) == 1
        assert real[None] == [value, ]
    elif type(value) is list:
        assert len(real) == 1
        assert real[None] == value
    else:
        assert len(real) == len(value)
        for k, v in value.items():
            assert k in real
            for effective, expected in zip(real[k], v):
                assert effective == expected
    # AND WHEN (idempotency)
    setattr(node, key, value)
    # THEN
    real = getattr(node, key)
    if type(value) is str or type(value) is type(None):
        assert len(real[None]) == 1
        assert real[None] == [value, ]
    elif type(value) is list:
        assert len(real) == 1
        assert real[None] == value
    else:
        assert len(real) == len(value)
        for k, v in value.items():
            assert k in real
            for effective, expected in zip(real[k], v):
                assert effective == expected


@pytest.mark.parametrize('cls', [
    Property, Entity, Attribute,
    ])
@pytest.mark.parametrize('key', [
    'uri',
    ])
@pytest.mark.parametrize('values', [
    "test", "", None,
    ["test", ], ["test1", "test2", ], ["", ], [None, ],
    ])
def test_element_documentable_mixin_repeat(cls, key, values):
    # GIVEN
    node = cls()
    assert len(getattr(node, key)) == 0
    # WHEN
    setattr(node, key, values)
    # THEN
    real = getattr(node, key)
    if type(values) is str or values is None:
        assert len(real) == 1
        assert real == [values, ]
    else:  # types(values) is list
        assert type(real) is list
        assert len(real) == len(values)
        for r in real:
            assert r in values
    # AND WHEN (idempotency)
    setattr(node, key, values)
    # THEN
    real = getattr(node, key)
    if type(values) is str or values is None:
        assert len(real) == 1
        assert real == [values, ]
    else:  # types(values) is list
        assert type(real) is list
        assert len(real) == len(values)
        for r in real:
            assert r in values


@pytest.mark.parametrize('cls', [
    Property, Entity, Attribute,
    ])
@pytest.mark.parametrize('key', [
    'uri',
    ])
@pytest.mark.parametrize('value', [
    {}, {'key': 'value'},
    ])
def test_TypeError_documentable_mixin_repeat(cls, key, value):
    node = cls()  # GIVEN
    try:
        setattr(node, key, value)  # WHEN
        assert False
    except TypeError:
        assert True


@pytest.mark.parametrize('cls', [
    Property, Attribute,
    ])
@pytest.mark.parametrize('key', [
    'type',
    ])
@pytest.mark.parametrize('value', [
    "test", "", None,
    ])
def test_element_documentable_mixin_unique(cls, key, value):
    # GIVEN
    node = cls()
    assert getattr(node, key) is None
    # WHEN
    setattr(node, key, value)
    # THEN
    real = getattr(node, key)
    assert (type(real) is str) or (real is None)
    assert real == value
    # AND WHEN (idempotency)
    setattr(node, key, value)
    # THEN
    real = getattr(node, key)
    assert (type(real) is str) or (real is None)
    assert real == value


@pytest.mark.parametrize('cls', [
    Property, Attribute,
    ])
@pytest.mark.parametrize('key', [
    'type',
    ])
@pytest.mark.parametrize('value', [
    [], ["test", ], ["test1", "test2", ], [None, ],
    {}, {'key': 'value'},
    ])
def test_TypeError_documentable_mixin_unique(cls, key, value):
    node = cls()  # GIVEN
    try:
        setattr(node, key, value)  # WHEN
        assert False
    except TypeError:
        assert True


@pytest.mark.parametrize('cls', [
    Attribute,
    ])
def test_element_repeatable_mixin_init_set_then_del(cls):
    # GIVEN
    node = cls()
    assert node.min == 0
    assert node.max is None
    # WHEN, THEN
    node.min = 1
    assert node.min == 1
    node.max = 42
    assert node.max == 42
    # AND WHEN, THEN
    del node.min
    assert node.min == 0
    del node.max
    assert node.max is None


@pytest.mark.parametrize('cls', [
    Attribute,
    ])
def test_element_repeatable_mixin_max_lt_min_nonsense(cls):
    # GIVEN
    node = cls()
    node.min = 42
    node.max = 1
    assert node.min == 42
    assert node.max == 42


@pytest.mark.parametrize('cls', [
    Attribute,
    ])
def test_element_repeatable_mixin_min_gt_max_nonsense(cls):
    # GIVEN
    node = cls()
    node.max = 1
    node.min = 42
    assert node.min == 1
    assert node.max == 1


@pytest.mark.parametrize('cls', [
    Attribute,
    ])
def test_element_repeatable_mixin_negative_nonsense(cls):
    # GIVEN
    node = cls()
    node.min = -1
    assert node.min == 0
    node.max = -1
    assert node.max == 1


@pytest.mark.parametrize('cls', [
    Attribute,
    ])
def test_element_repeatable_mixin_set_min_none(cls):
    # GIVEN
    node = cls()
    assert node.min == 0
    # WHEN
    node.min = None
    # THEN
    assert node.min == 0


@pytest.mark.parametrize('cls', [
    Attribute,
    ])
def test_element_repeatable_mixin_set_max_none(cls):
    # GIVEN
    node = cls()
    assert node.max is None
    # WHEN
    node.max = None
    # THEN
    assert node.max is None
