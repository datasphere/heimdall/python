# -*- coding: utf-8 -*-
import pytest
import heimdall
from heimdall.util import (
    get_node, get_nodes, get_root,
    get_language,
    refactor_relationship,
    merge_l10n_attributes,
    merge_properties,
    delete_unused_properties,
    )
from .stubs import *
from xml.etree.ElementTree import Element


@pytest.mark.parametrize(('db'), [
    like_rilly_empty_db,
    as_good_as_empty_db,
    only_1_item_pea_db,
    mini_consistent_db,
    menagerie_db,
    ])
def test_get_root(db):
    root = get_root(db())
    assert root is not None
    assert root.tag == 'hera'


@pytest.mark.parametrize(('tag', 'expected'), [
    ('items', 1), ('entities', 1), ('properties', 1),
    ('item', 0), ('entity', 0),  ('property', 0),
    ('attribute', 0), ('metadata', 0),
    ('whatever', 0),
    ])
def test_get_direct_nodes(tag, expected):
    tree = mini_consistent_db()
    nodes = get_nodes(tree, tag, direct=True)
    assert len(nodes) == expected


@pytest.mark.parametrize(('tag', 'expected'), [
    ('items', 1), ('entities', 1), ('properties', 1),
    ('item', 3), ('entity', 2),  ('property', 3),
    ('attribute', 2), ('metadata', 6),
    ('whatever', 0),
    ])
def test_get_indirect_nodes(tag, expected):
    tree = mini_consistent_db()
    nodes = get_nodes(tree, tag, direct=False)
    assert len(nodes) == expected


@pytest.mark.parametrize(('node', 'key', 'value', 'before', 'after'), [
    (Element('test'), 'key', 'value', None, 'value'),
    (Element('test'), 'key', None, None, None),
    (Element('test', key='value'), 'key', 'value', 'value', 'value'),
    (Element('test', key='value'), 'key', None, 'value', None),
    (Element('test', key='value'), 'key', 'other', 'value', 'other'),
    (Element('test', key='value'), 'other', 'value', None, 'value'),
    ])
def test_update_node_value(node, key, value, before, after):
    assert node.get(key) == before
    heimdall.util.tree.update_node_value(node, key, value)
    assert node.get(key) == after


@pytest.mark.parametrize(('db', 'expected'), [
    (menagerie_db, {
        'pet': (7, 9),
        'person': (4, 4),
        'relationship': (3, 9),
        }),
    ])
def test_menagerie_db(db, expected):
    tree = db()
    nb_items = sum([x[1] for x in expected.values()])
    nb_props = sum([x[0] for x in expected.values()])
    properties = heimdall.getProperties(tree)
    entities = heimdall.getEntities(tree)
    items = heimdall.getItems(tree)
    assert len(items) is nb_items
    assert len(entities) is len(expected)
    assert len(properties) is nb_props
    for eid, (nbprops, nbitems) in expected.items():
        props = [p for p in properties if p.get('id').startswith(f'{eid}.')]
        assert len(props) is nbprops
        entity = heimdall.getEntity(tree, lambda e: e.get('id') == eid)
        assert entity is not None
        nbattrs = expected[eid][0]
        assert len(heimdall.getAttributes(entity)) is nbattrs
        items_ = [i for i in items if i.get('eid') == eid]
        assert len(items_) is nbitems


@pytest.mark.parametrize(('db', 'eid', 'before', 'aid_after', 'pid_after'), [
    (menagerie_db, 'pet', {
         'pet.name_attr': 'en',
         'pet.name_fr_attr': 'fr',
         },
     'pet.name', 'pet.name'),
    (menagerie_db, 'pet', {
         'pet.name_attr': 'en',
         'pet.name_fr_attr': 'fr',
         },
     'pet.name', None),  # <-- missing property ID
    ])
def test_merge_l10n_attributes(db, eid, before, aid_after, pid_after):
    # GIVEN
    tree = db()
    entity = heimdall.getEntity(tree, lambda e: e.get('id') == eid)
    assert len(heimdall.getAttributes(entity)) is 7
    items = heimdall.getItems(tree, lambda n: n.get('eid') == eid)
    for item in items:
        assert len(heimdall.getMetadata(item, aid=aid_after)) is 0
    attr = heimdall.getAttribute(entity, lambda a: a.get('id') == aid_after)
    assert attr is None
    for aid in before.keys():
        attr = heimdall.getAttribute(entity, lambda a: a.get('id') == aid)
        assert attr is not None
        name = get_node(attr, 'name')
        assert name is not None
        assert get_language(name) is None
    # WHEN
    merge_l10n_attributes(tree, eid, before, pid_after, aid_after)
    # THEN
    assert len(heimdall.getAttributes(entity)) is 6
    # NOTE: check previous attributes were deleted
    for aid in before.keys():
        attr = heimdall.getAttribute(entity, lambda a: a.get('id') == aid)
        assert attr is None
    # NOTE: check new attribute was created, including previous attributes
    #       names and description in each language
    attr = heimdall.getAttribute(entity, lambda a: a.get('id') == aid_after)
    assert attr is not None
    assert attr.get('pid') == pid_after
    names = get_nodes(attr, 'name')
    assert len(names) is 2
    for name in names:
        assert get_language(name) is not None
    descriptions = get_nodes(attr, 'description')
    assert len(descriptions) is 2
    for description in descriptions:
        assert get_language(description) is not None
    # NOTE: check each item was updated
    for item in items:
        for aid in before.keys():
            metadata = heimdall.getMetadata(item, aid=aid)
            assert len(metadata) is 0
        metadata = heimdall.getMetadata(item, aid=aid_after)
        assert len(metadata) is 1 or len(metadata) is 2
        for m in metadata:
            assert get_language(m) is not None


@pytest.mark.parametrize(('db', 'eid', 'before', 'aid_after', 'pid_after'), [
    (menagerie_db, 'pet', {
         'pet.name_attr': 'en',
         'pet.name_fr_attr': 'fr',
         },
     'pet.name_attr', 'pet.name'),
    ])
def test_merge_l10n_attributes_into_existing_attr(
        db, eid, before, aid_after, pid_after):
    # GIVEN
    tree = db()
    entity = heimdall.getEntity(tree, lambda e: e.get('id') == eid)
    assert len(heimdall.getAttributes(entity)) is 7
    # WHEN
    merge_l10n_attributes(tree, eid, before, aid=aid_after, pid=pid_after)
    # THEN
    assert len(heimdall.getAttributes(entity)) is 6
    # NOTE: check attributes changes
    aid = 'pet.name_fr_attr'
    attr = heimdall.getAttribute(entity, lambda a: a.get('id') == aid)
    assert attr is None
    aid = 'pet.name_attr'
    attr = heimdall.getAttribute(entity, lambda a: a.get('id') == aid)
    assert attr is not None
    assert attr.get('pid') == 'pet.name'
    names = get_nodes(attr, 'name')
    assert len(names) is 2
    for name in names:
        assert get_language(name) is not None
    descriptions = get_nodes(attr, 'description')
    assert len(descriptions) is 2
    for description in descriptions:
        assert get_language(description) is not None
    # NOTE: check each item was updated
    items = heimdall.getItems(tree, lambda n: n.get('eid') == eid)
    for item in items:
        metadata = heimdall.getMetadata(item, aid=aid_after)
        assert len(metadata) is 1 or len(metadata) is 2
        for m in metadata:
            assert get_language(m) is not None


@pytest.mark.parametrize(('db', 'eid', 'before', 'pid_after', 'aid_after'), [
    (menagerie_db, 'pet', {
         'pet.name_attr': 'en',
         'pet.name_fr_attr': 'fr',
         },
     'pet.name', 'pet.name'),
    ])
def test_merge_l10n_attributes_no_child(db, eid, before, pid_after, aid_after):
    # GIVEN
    tree = db()
    entity = heimdall.getEntity(tree, lambda e: e.get('id') == eid)
    for a in heimdall.getAttributes(entity):
        node = get_node(a, 'type')
        if node is not None:
            a.remove(node)
        node = get_node(a, 'name')
        if node is not None:
            a.remove(node)
        node = get_node(a, 'description')
        if node is not None:
            a.remove(node)
    assert len(heimdall.getAttributes(entity)) is 7
    # WHEN
    merge_l10n_attributes(tree, eid, before, pid_after, aid_after)
    # THEN
    assert len(heimdall.getAttributes(entity)) is 6
    # NOTE: check previous attributes were deleted
    for aid in before.keys():
        attr = heimdall.getAttribute(entity, lambda a: a.get('id') == aid)
        assert attr is None
    # NOTE: check new attribute was created, but no type/name/desc was added
    attr = heimdall.getAttribute(entity, lambda a: a.get('id') == aid_after)
    assert attr is not None
    assert attr.get('pid') == pid_after
    node = get_node(attr, 'type')
    assert node is None
    node = get_node(attr, 'name')
    assert node is None
    node = get_node(attr, 'description')
    assert node is None


@pytest.mark.parametrize(('db', 'eid', 'before', 'pid_after', 'aid_after'), [
    (menagerie_db, 'pet', {
         'pet.name_attr': 'en',
         'pet.name_fr_attr': 'fr',
         },
     'pet.name', 'pet.name'),
    ])
def test_merge_l10n_attributes_but_dont_update_items(
        db, eid, before, pid_after, aid_after):
    # GIVEN
    tree = db()
    entity = heimdall.getEntity(tree, lambda e: e.get('id') == eid)
    for a in heimdall.getAttributes(entity):
        type = get_node(a, 'type')
        if type is not None:
            a.remove(type)
    # WHEN
    merge_l10n_attributes(
            tree, eid, before,
            pid_after, aid_after,
            update_items=False)  # NOTE: in this test we DON'T update items
    # THEN
    assert len(heimdall.getAttributes(entity)) is 6
    # NOTE: check previous attributes were deleted
    for aid in before.keys():
        attr = heimdall.getAttribute(entity, lambda a: a.get('id') == aid)
        assert attr is None
    # NOTE: check each item was updated
    items = heimdall.getItems(tree, lambda n: n.get('eid') == eid)
    for item in items:
        metadata = heimdall.getMetadata(item, aid=aid_after)
        assert len(metadata) is 0
        metadata = heimdall.getMetadata(item, aid='pet.name_attr')
        assert len(metadata) is 1


@pytest.mark.parametrize(('db', 'eid'), [
    (menagerie_db, 'pets'),
    (menagerie_db, 'pouet'),
    (menagerie_db, ''),
    ])
def test_merge_l10n_attributes_error_if_eid_not_found(db, eid):
    # GIVEN
    tree = db()
    entity = heimdall.getEntity(tree, lambda e: e.get('id') == eid)
    assert entity is None
    # WHEN
    try:
        merge_l10n_attributes(tree, eid, {'what': 'ever'}, '~', '~')
        assert False
    except ValueError as ex:
        assert eid in repr(ex)


@pytest.mark.parametrize(('db', 'eid', 'languages'), [
    (menagerie_db, 'pet', {'pets': 'en'}),
    (menagerie_db, 'pet', {'pouet': 'fr'}),
    (menagerie_db, 'pet', {'': 'en'}),
    ])
def test_merge_l10n_attributes_error_if_aid_not_found(db, eid, languages):
    # GIVEN
    tree = db()
    entity = heimdall.getEntity(tree, lambda e: e.get('id') == eid)
    assert entity is not None
    # WHEN
    try:
        merge_l10n_attributes(tree, eid, languages, 'whatever', 'whatever')
        assert False
    except ValueError as ex:
        assert list(languages.keys())[0] in repr(ex)


@pytest.mark.parametrize(('db', 'eid', 'languages'), [
    (menagerie_db, 'pet', {'pet.name_fr_attr': 'fr', 'pets': 'en'}),
    (menagerie_db, 'pet', {'pet.name_attr': 'en', 'pouet': 'fr'}),
    (menagerie_db, 'pet', {'pet.name_fr_attr': 'fr', '': 'en'}),
    ])
def test_merge_l10n_attributes_error_if_last_aid_not_found(db, eid, languages):
    # GIVEN
    tree = db()
    entity = heimdall.getEntity(tree, lambda e: e.get('id') == eid)
    assert entity is not None
    aid = list(languages.keys())[-1]
    # WHEN
    try:
        merge_l10n_attributes(tree, eid, languages, 'whatever', aid)
        assert False
    except ValueError as ex:
        assert list(languages.keys())[-1] in repr(ex)


@pytest.mark.parametrize(('db', 'eid', 'before', 'pid'), [
    (menagerie_db, 'pet', {
         'pet.name_attr': 'en',
         'pet.name_fr_attr': 'fr',
         },
     'pet.name'),
    ])
def test_merge_l10n_attributes_error_if_types_conflict(db, eid, before, pid):
    # GIVEN
    tree = db()
    entity = heimdall.getEntity(tree, lambda e: e.get('id') == eid)
    aid = list(before.keys())[0]
    attribute = heimdall.getAttribute(entity, lambda a: a.get('id') == aid)
    attribute.type = 'a_different_type'
    # WHEN ... THEN EXCEPT
    try:
        merge_l10n_attributes(tree, eid, before, pid, 'whatever')
        assert False
    except ValueError as ex:
        assert True


@pytest.mark.parametrize(('db', 'expected'), [
    (menagerie_db, {
        'pet': (7, 9),
        'person': (4, 4),
        'relationship': (3, 9),
        }),
    ])
def test_delete_unused_properties(db, expected):
    # GIVEN
    tree = db()
    nb_props = sum([x[0] for x in expected.values()])
    nb_items = sum([x[1] for x in expected.values()])
    assert len(heimdall.getProperties(tree)) is nb_props
    assert len(heimdall.getEntities(tree)) is len(expected)
    assert len(heimdall.getItems(tree)) is nb_items
    # WHEN
    merge_l10n_attributes(tree, 'pet', {
        'pet.name_attr': 'en',
        'pet.name_fr_attr': 'fr',
        },
        'pet.name', 'pet.name')
    # THEN
    assert len(heimdall.getProperties(tree)) is nb_props
    # ..AND WHEN
    delete_unused_properties(tree)
    # THEN
    assert len(heimdall.getProperties(tree)) is nb_props-1
    assert len(heimdall.getEntities(tree)) is len(expected)
    assert len(heimdall.getItems(tree)) is nb_items


@pytest.mark.parametrize(('db', 'expected'), [
    (like_rilly_empty_db, 0),
    (as_good_as_empty_db, 0),
    (only_1_item_pea_db, 1),
    (mini_consistent_db, 3),
    (menagerie_db, 14),
    ])
def test_delete_unused_properties_non_relational(db, expected):
    # GIVEN
    tree = db()
    # NOTE: remove all entities to make db non-relational
    #  (we don't bother to remove eid and aid from items)
    for e in heimdall.getEntities(tree):
        heimdall.deleteEntity(tree, lambda n: n.get('id') == e.get('id'))
    assert len(heimdall.getProperties(tree)) is expected
    assert len(heimdall.getEntities(tree)) is 0
    # NOTE: add some new properties (they will remain unused)
    pids = ['whatever', 'i dont care', 'ill soon be dead', 'anyways']
    for pid in pids:
        heimdall.createProperty(tree, pid)
    assert len(heimdall.getProperties(tree)) is expected + len(pids)
    # WHEN
    delete_unused_properties(tree, relational=False)
    # THEN
    assert len(heimdall.getProperties(tree)) is expected


def test_delete_unused_properties_inconsistent_attribute():
    # GIVEN (a tree with an attribute pointing on missing pid)
    PID = 'does_not_exist'
    tree = heimdall.util.tree.create_empty_tree()
    e = heimdall.createEntity(tree, 'e')
    heimdall.createAttribute(e, pid=PID)
    # WHEN
    delete_unused_properties(tree)
    # THEN (tree is left untouched)
    assert len(heimdall.getProperties(tree)) is 0
    assert len(heimdall.getEntities(tree)) is 1
    assert heimdall.getAttribute(e, lambda n: n.get('pid') == PID) is not None
    assert len(heimdall.getItems(tree)) is 0


@pytest.mark.parametrize(('db', 'expected'), [
    (menagerie_db, {
        'pet': (7, 9),
        'person': (4, 4),
        'relationship': (3, 9),
        }),
    ])
def test_refactor_relationship(db, expected):
    tree = db()
    REL = 'relationship'
    R = heimdall.getEntity(tree, lambda e: e.get('id') == REL)
    assert R is not None
    nb_items = sum([x[1] for x in expected.values()])
    nb_props = sum([x[0] for x in expected.values()])
    assert len(heimdall.getItems(tree)) is nb_items
    assert len(heimdall.getEntities(tree)) is len(expected)
    assert len(heimdall.getProperties(tree)) is nb_props
    # GIVEN
    relationship = {
        'eid': REL,  # entity we want to get rid of
        'source': f'{REL}.owner',
        'target': f'{REL}.pet',
        }
    eid = 'person'  # repeatable attribute-to-be will go in this entity
    euid = 'person.name'  # unique id property for this entity
    pid = 'pets'  # id of the new property to be created for this entity
    # WHEN
    refactor_relationship(tree, relationship, eid, euid, pid)
    # THEN
    relationship_nbitems = expected[REL][1]
    relationship_nbprops = expected[REL][0]
    # NOTE: check that all items of eid='REL' don't exist anymore
    assert len(heimdall.getItems(tree)) is nb_items-relationship_nbitems
    assert len(heimdall.getItems(tree, lambda n: n.get('eid') == REL)) is 0
    # NOTE: check that entity REL doesn't exist anymore
    assert len(heimdall.getEntities(tree)) is len(expected)-1
    # NOTE: check that property 'pets' was created
    assert len(heimdall.getProperties(tree)) is nb_props+1
    # ... AND WHEN
    delete_unused_properties(tree)
    # THEN
    assert len(heimdall.getProperties(tree)) is nb_props+1-relationship_nbprops


@pytest.mark.parametrize(('db', 'expected'), [
    (menagerie_db, {
        'pet': (7, 9),
        'person': (4, 4),
        'relationship': (3, 9),
        }),
    ])
def test_refactor_relationship_no_cleanup(db, expected):
    tree = db()
    REL = 'relationship'
    R = heimdall.getEntity(tree, lambda e: e.get('id') == REL)
    assert R is not None
    nb_items = sum([x[1] for x in expected.values()])
    nb_props = sum([x[0] for x in expected.values()])
    assert len(heimdall.getItems(tree)) is nb_items
    assert len(heimdall.getEntities(tree)) is len(expected)
    assert len(heimdall.getProperties(tree)) is nb_props
    # GIVEN
    relationship = {
        'eid': REL,  # entity we want to get rid of
        'source': f'{REL}.owner',
        'target': f'{REL}.pet',
        }
    eid = 'person'  # repeatable attribute-to-be will go in this entity
    euid = 'person.name'  # unique id property for this entity
    pid = 'pets'  # id of the new property to be created for this entity
    # WHEN
    refactor_relationship(tree, relationship, eid, euid, pid, cleanup=False)
    # THEN
    relationship_nbitems = expected[REL][1]
    relationship_nbprops = expected[REL][0]
    # NOTE: check that all items of eid='REL' are still there
    assert len(heimdall.getItems(tree)) is nb_items
    # NOTE: check that entity REL still exists
    assert len(heimdall.getEntities(tree)) is len(expected)
    assert len(heimdall.getEntity(tree, lambda e: e == R)) is not None
    # NOTE: check that property 'pets' was created
    assert len(heimdall.getProperties(tree)) is nb_props+1


@pytest.mark.parametrize(('db', 'properties', 'old', 'now'), [
    (only_1_item_pea_db,
     {'uid': 'uuid', },
     ['uid', ],
     ['uuid', ],
     ),
    (mini_consistent_db,
     {'uid': 'dc:identifier', 'namez': None, 'value': 'value', },
     ['uid', 'value', ],
     ['dc:identifier', 'value', ],
     ),
    (menagerie_db,
     {
        'person.name':  'dc:title', 'pet.name':  'dc:title',
        'person.sex':   'dc:type',  'pet.sex':   'dc:type',
        'person.birth': 'dc:date',  'pet.birth': 'dc:date',
        'person.death': 'dc:date',  'pet.death': 'dc:date',
     },
     [
         'person.name', 'person.sex', 'person.birth', 'person.death',
         'pet.name', 'pet.sex', 'pet.birth', 'pet.death',
     ],
     ['dc:title', 'dc:type', 'dc:date', ],
     ),
    ])
def test_merge_properties(db, properties, old, now):
    # GIVEN
    tree = db()
    # WHEN
    merge_properties(tree, properties)
    # THEN
    # for the old set of properties
    for pid in old:
        # assert property still exists ...
        p = heimdall.getProperty(tree, lambda n: n.get('id') == pid)
        assert p is not None
        # ... but if it has not been kept untouched ...
        if pid in now:
            continue
        # ... it is not used anymore, neither in items, ...
        for i in heimdall.getItems(tree):
            assert len(heimdall.getMetadata(i, pid=pid)) is 0
        # ... not in entities
        for e in heimdall.getEntities(tree):
            a = heimdall.getAttribute(e, lambda n: n.get('pid') == pid)
            assert a is None
    # for the new set of used properties ...
    for pid in now:
        # we can't assert property has been created,
        # as merge_properties doesn't createi or fail it if missing
        # but we can assert is used, by items metadata, ...
        found = 0
        for i in heimdall.getItems(tree):
            metas = heimdall.getMetadata(i, pid=pid)
            found = max(found, len(metas))
        assert found > 0
        # ... as long as by entities attributes
        found = 0
        for e in heimdall.getEntities(tree):
            attrs = heimdall.getAttributes(e, lambda n: n.get('pid') == pid)
            found = max(found, len(attrs))
        assert found > 0


@pytest.mark.parametrize(('db'), [
    (like_rilly_empty_db),
    (as_good_as_empty_db),
    (only_1_item_pea_db),
    (mini_consistent_db),
    (menagerie_db),
    ])
def test_merge_properties_does_nothing_if_pid_does_not_exist(db):
    # GIVEN
    tree = db()
    items = heimdall.getItems(tree)
    ents = heimdall.getEntities(tree)
    props = heimdall.getProperties(tree)
    # WHEN
    merge_properties(tree, {'nonexistent': 'whatever'})
    # THEN
    assert all(x == y for x, y in zip(items, heimdall.getItems(tree)))
    assert all(x == y for x, y in zip(ents, heimdall.getEntities(tree)))
    assert all(x == y for x, y in zip(props, heimdall.getProperties(tree)))
