# -*- coding: utf-8 -*-
import pytest
import heimdall
from ..stubs import *


# #############
# # GET items #
# #############

@pytest.mark.parametrize(('db', 'expected'), [
    (like_rilly_empty_db, 0),
    (as_good_as_empty_db, 0),
    (only_1_item_pea_db, 1),
    (mini_consistent_db, 3),
    ])
def test_getItems(db, expected):
    results = heimdall.getItems(db())
    assert results is not None
    assert len(results) == expected


@pytest.mark.parametrize(('db'), [
    (like_rilly_empty_db),
    (as_good_as_empty_db),
    (only_1_item_pea_db),
    (mini_consistent_db),
    ])
def test_getItem_fails_if_no_filter(db):
    try:
        heimdall.getItem(db())  # always raises TypeError, so ...
        assert False  # ... we should never reach this assert
    except TypeError:
        assert True


@pytest.mark.parametrize(('db'), [
    (like_rilly_empty_db),
    (as_good_as_empty_db),
    (only_1_item_pea_db),
    (mini_consistent_db),
    ])
def test_getItem_no_result(db):
    result = heimdall.getItem(db(), lambda x: False)
    assert result is None


@pytest.mark.parametrize(('db'), [
    (mini_consistent_db),
    ])
def test_getItem_too_many_results(db):
    try:
        heimdall.getItem(db(), lambda x: True)
        assert False  # we should never reach this
    except IndexError:
        assert True


@pytest.mark.parametrize(('db', 'uid'), [
    (only_1_item_pea_db, '1337'),
    (mini_consistent_db, '1337'),
    ])
def test_getItem_success(db, uid):

    def by_uid(item):
        return heimdall.getValue(item, pid='uid') == uid
    item = heimdall.getItem(db(), by_uid)
    assert item is not None
    assert heimdall.getValue(item, pid='uid') == uid


# ################
# # CREATE items #
# ################

@pytest.mark.parametrize(('db'), [
    (like_rilly_empty_db),
    (as_good_as_empty_db),
    (only_1_item_pea_db),
    (mini_consistent_db),
    ])
def test_createItem_no_metadata_no_problem(db):
    tree = db()
    size = len(heimdall.getItems(tree))
    heimdall.createItem(tree)
    assert len(heimdall.getItems(tree)) == size+1


@pytest.mark.parametrize(('db'), [
    (like_rilly_empty_db),
    (as_good_as_empty_db),
    (only_1_item_pea_db),
    (mini_consistent_db),
    ])
def test_createItem_no_entity_no_problem(db):
    tree = db()
    size = len(heimdall.getItems(tree))
    heimdall.createItem(tree, eid='42')
    assert len(heimdall.getItems(tree)) == size+1


@pytest.mark.parametrize(('db'), [
    (like_rilly_empty_db),
    (as_good_as_empty_db),
    (only_1_item_pea_db),
    (mini_consistent_db),
    ])
def test_createItem_no_property_no_problem(db):
    tree = db()
    size = len(heimdall.getItems(tree))
    heimdall.createItem(tree, name='Chirpy', type='birb')
    assert len(heimdall.getItems(tree)) == size+1


@pytest.mark.parametrize(('db'), [
    (like_rilly_empty_db),
    (as_good_as_empty_db),
    (only_1_item_pea_db),
    (mini_consistent_db),
    ])
def test_createItem_more_than_once(db):
    tree = db()
    size = len(heimdall.getItems(tree))
    heimdall.createItem(tree, name='x', type='y')
    heimdall.createItem(tree, name='x', type='y')
    heimdall.createItem(tree, name='x', type='y')
    assert len(heimdall.getItems(tree)) == size+3


@pytest.mark.parametrize(('db'), [
    (like_rilly_empty_db),
    (as_good_as_empty_db),
    (only_1_item_pea_db),
    (mini_consistent_db),
    ])
def test_createItem_is_consistent(db):
    tree = db()
    size = len(heimdall.getItems(tree))
    heimdall.createItem(tree, name='Chirpy', eid='pet', type='birb')
    assert len(heimdall.getItems(tree)) == size+1

    def by_name(item):
        return heimdall.getValue(item, pid='name') == 'Chirpy'

    item = heimdall.getItem(tree, by_name)
    assert item is not None
    assert item.get('eid') == 'pet'
    assert heimdall.getValue(item, pid='name') == 'Chirpy'
    assert heimdall.getValue(item, pid='type') == 'birb'
    assert len(heimdall.getMetadata(item)) == 2


@pytest.mark.parametrize(('db'), [
    (like_rilly_empty_db),
    (as_good_as_empty_db),
    (only_1_item_pea_db),
    (mini_consistent_db),
    ])
def test_createItem_i18n(db):
    tree = db()
    size = len(heimdall.getItems(tree))
    heimdall.createItem(
        tree,
        name={'en': 'Chirpy', 'fr': 'Cui-Cui', },
        type={'en': 'birb', 'fr': 'wazo', },
        eid='pet')
    assert len(heimdall.getItems(tree)) == size+1

    def by_name(item):
        names = heimdall.getValues(item, pid='name')
        return "Chirpy" in names

    item = heimdall.getItem(tree, by_name)
    assert item is not None
    assert item.get('eid') == 'pet'
    names = heimdall.getValues(item, pid='name')
    assert len(names) == 2
    assert 'Chirpy' in names
    assert 'Cui-Cui' in names
    types = heimdall.getValues(item, pid='type')
    assert len(types) == 2
    assert 'birb' in types
    assert 'wazo' in types


# ################
# # DELETE items #
# ################

@pytest.mark.parametrize(('db'), [
    (like_rilly_empty_db),
    (as_good_as_empty_db),
    (only_1_item_pea_db),
    (mini_consistent_db),
    ])
def test_deleteItem_fails_if_no_filter(db):
    try:
        heimdall.deleteItem(db())  # always raises TypeError, so ...
        assert False  # ... we should never reach this assert
    except TypeError:
        assert True


@pytest.mark.parametrize(('db', 'expected'), [
    (like_rilly_empty_db, 0),
    (as_good_as_empty_db, 0),
    (only_1_item_pea_db, 1),
    (mini_consistent_db, 3),
    ])
def test_deleteItem_does_nothing_if_filter_does_nothing(db, expected):
    tree = db()
    assert len(heimdall.getItems(tree)) == expected
    heimdall.deleteItem(tree, lambda x: False)
    assert len(heimdall.getItems(tree)) == expected


@pytest.mark.parametrize(('db'), [
    (like_rilly_empty_db),
    (as_good_as_empty_db),
    ])
def test_deleteItem_does_nothing_on_empty_db(db):
    tree = db()
    assert len(heimdall.getItems(tree)) == 0
    heimdall.deleteItem(tree, lambda x: True)
    assert len(heimdall.getItems(tree)) == 0


@pytest.mark.parametrize(('db', 'before', 'after'), [
    (only_1_item_pea_db, 1, 0),
    (mini_consistent_db, 3, 2),
    ])
def test_deleteItem_deletes_1_success(db, before, after):
    tree = db()
    assert len(heimdall.getItems(tree)) == before

    def by_name(item):
        return heimdall.getValue(item, pid='name') == 'shibboleet'

    heimdall.deleteItem(tree, by_name)
    assert len(heimdall.getItems(tree)) == after


@pytest.mark.parametrize(('db'), [
    (mini_consistent_db),
    ])
def test_deleteItem_deletes_more_than_1_success(db):
    tree = db()
    assert len(heimdall.getItems(tree)) >= 2
    heimdall.deleteItem(tree, lambda x: True)
    assert len(heimdall.getItems(tree)) == 0
