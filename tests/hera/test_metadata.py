# -*- coding: utf-8 -*-
import pytest
import heimdall
from heimdall.elements import Builder
from xml.etree.ElementTree import XMLParser, fromstring


EMPTY = """<item />"""
EMPTY_EID = """<item eid='entity'/>"""
ONLY_ONE = """
<item>
  <metadata pid='property'>value</metadata>
</item>"""
ONLY_ONE_EID = """
<item eid='entity'>
  <metadata aid='attribute'>value</metadata>
</item>"""
ONLY_ONE_BOTH = """
<item eid='entity'>
  <metadata aid='attribute' pid='property'>value</metadata>
</item>"""
COMBINASHUN = """
<item eid='entity'>
  <metadata aid='attribute'>aid_only</metadata>
  <metadata pid='property'>pid_only</metadata>
  <metadata aid='attribute' pid='property'>both</metadata>
  <metadata aid='attribute2' pid='property' custom='value'>both2</metadata>
  <metadata aid='attribute2' custom='value'>aid2_only</metadata>
</item>"""
COMBI_NO_VALUE = """
<item eid='entity'>
  <metadata aid='attribute'></metadata>
  <metadata pid='property'></metadata>
  <metadata aid='attribute' pid='property'></metadata>
  <metadata aid='attribute2' pid='property'></metadata>
  <metadata aid='attribute2'></metadata>
</item>"""
LOCALIZED = """
<item eid='pet'>
  <metadata pid='dc:title' xml:lang='fr'>Animal de compagnie</metadata>
  <metadata pid='dc:title' xml:lang='fr'>Meuble à poils</metadata>
  <metadata pid='dc:title' xml:lang='en'>Pet</metadata>
  <metadata pid='dc:title' xml:lang='ar'>حيوان أليف</metadata>
  <metadata pid='dc:title' xml:lang='zh'>宠儿</metadata>
  <metadata pid='dc:title' xml:lang='zh'>寵物</metadata>
  <metadata pid='dc:description' xml:lang='fr'>Un p'tit cœur avec du poil autour</metadata>
  <metadata pid='dc:description' xml:lang='fr' floof='high'>Animal domestique à poils, à plumes, à écailles, etc.</metadata>
  <metadata pid='dc:description' xml:lang='en'>Companion animal</metadata>
  <metadata pid='dc:description' xml:lang='emoji' floof='high'>🐶🐱🐭🐹🐰🦜🐠🐢</metadata>
  <metadata pid='dc:title'>Default title</metadata>
  <metadata pid='dc:description'>Default description</metadata>
</item>"""  # nopep8: E501


# ################
# # GET metadata #
# # (and values) #
# ################


@pytest.mark.parametrize(('item', 'expected'), [
    (EMPTY, 0),
    (EMPTY_EID, 0),
    (ONLY_ONE, 1),
    (ONLY_ONE_EID, 1),
    (COMBINASHUN, 5),
    (LOCALIZED, 12),
    ])
def test_getMetadata_no_option(item, expected):
    item = fromstring(item, XMLParser(target=Builder()))
    metadata = heimdall.getMetadata(item)
    assert len(metadata) is expected
    for m in metadata:
        assert m.text is not None


@pytest.mark.parametrize(('item', 'expected'), [
    (COMBI_NO_VALUE, 5),
    ])
def test_getMetadata_no_option_no_value(item, expected):
    item = fromstring(item, XMLParser(target=Builder()))
    metadata = heimdall.getMetadata(item)
    assert len(metadata) is expected
    for m in metadata:
        assert m.text is None


@pytest.mark.parametrize(('item', 'aid', 'expected'), [
    (EMPTY, 'whatever', 0),
    (EMPTY, None, 0),
    (EMPTY_EID, 'whatever', 0),
    (EMPTY_EID, None, 0),
    (ONLY_ONE, 'whatever', 0),
    (ONLY_ONE, None, 1),
    (ONLY_ONE_EID, 'whatever', 0),
    (ONLY_ONE_EID, None, 0),
    (ONLY_ONE_EID, 'attribute', 1),
    (ONLY_ONE_BOTH, 'whatever', 0),
    (ONLY_ONE_BOTH, None, 0),
    (ONLY_ONE_BOTH, 'attribute', 1),
    (ONLY_ONE_BOTH, 'property', 0),
    (COMBINASHUN, 'whatever', 0),
    (COMBINASHUN, None, 1),
    (COMBINASHUN, 'attribute', 2),
    (COMBINASHUN, 'attribute2', 2),
    (COMBINASHUN, 'property', 0),
    (LOCALIZED, 'whatever', 0),
    (LOCALIZED, None, 12),
    (LOCALIZED, 'dc:title', 0),
    ])
def test_getMetadata_by_aid(item, aid, expected):
    item = fromstring(item, XMLParser(target=Builder()))
    metadata = heimdall.getMetadata(item, aid=aid)
    assert len(metadata) is expected
    for m in metadata:
        assert m.get('aid') == aid
        assert m.text is not None


@pytest.mark.parametrize(('item', 'aid', 'expected'), [
    (COMBI_NO_VALUE, 'whatever', 0),
    (COMBI_NO_VALUE, None, 1),
    (COMBI_NO_VALUE, 'attribute', 2),
    (COMBI_NO_VALUE, 'attribute2', 2),
    (COMBI_NO_VALUE, 'property', 0),
    ])
def test_getMetadata_by_aid_empty_value(item, aid, expected):
    item = fromstring(item, XMLParser(target=Builder()))
    metadata = heimdall.getMetadata(item, aid=aid)
    assert len(metadata) is expected
    for m in metadata:
        assert m.get('aid') == aid
        assert m.text is None


@pytest.mark.parametrize(('item', 'pid', 'expected'), [
    (EMPTY, 'whatever', 0),
    (EMPTY, None, 0),
    (EMPTY_EID, 'whatever', 0),
    (EMPTY_EID, None, 0),
    (ONLY_ONE, 'whatever', 0),
    (ONLY_ONE, None, 0),
    (ONLY_ONE, 'property', 1),
    (ONLY_ONE_EID, 'whatever', 0),
    (ONLY_ONE_EID, None, 1),
    (ONLY_ONE_EID, 'attribute', 0),
    (ONLY_ONE_BOTH, 'whatever', 0),
    (ONLY_ONE_BOTH, None, 0),
    (ONLY_ONE_BOTH, 'attribute', 0),
    (ONLY_ONE_BOTH, 'property', 1),
    (COMBINASHUN, 'whatever', 0),
    (COMBINASHUN, None, 2),
    (COMBINASHUN, 'attribute', 0),
    (COMBINASHUN, 'attribute2', 0),
    (COMBINASHUN, 'property', 3),
    (LOCALIZED, 'whatever', 0),
    (LOCALIZED, 'whatever', 0),
    (LOCALIZED, None, 0),
    (LOCALIZED, 'dc:title', 7),
    (LOCALIZED, 'dc:description', 5),
    (LOCALIZED, 'title', 0),
    (LOCALIZED, 'description', 0),
    (LOCALIZED, 'dc', 0),
    (LOCALIZED, ':', 0),
    ])
def test_getMetadata_by_pid(item, pid, expected):
    item = fromstring(item, XMLParser(target=Builder()))
    metadata = heimdall.getMetadata(item, pid=pid)
    assert len(metadata) is expected
    for m in metadata:
        assert m.get('pid') == pid
        assert m.text is not None


@pytest.mark.parametrize(('item', 'pid', 'expected'), [
    (COMBI_NO_VALUE, 'whatever', 0),
    (COMBI_NO_VALUE, None, 2),
    (COMBI_NO_VALUE, 'attribute', 0),
    (COMBI_NO_VALUE, 'attribute2', 0),
    (COMBI_NO_VALUE, 'property', 3),
    ])
def test_getMetadata_by_pid_empty_value(item, pid, expected):
    item = fromstring(item, XMLParser(target=Builder()))
    metadata = heimdall.getMetadata(item, pid=pid)
    assert len(metadata) is expected
    for m in metadata:
        assert m.get('pid') == pid
        assert m.text is None


@pytest.mark.parametrize(('item', 'pid', 'aid', 'expected'), [
    (EMPTY, 'whatever', 'whatever', 0),
    (EMPTY, None, None, 0),
    (EMPTY, 'whatever', None, 0),
    (EMPTY, None, 'whatever', 0),
    (EMPTY_EID, 'whatever', 'whatever', 0),
    (EMPTY_EID, None, None, 0),
    (EMPTY_EID, 'whatever', None, 0),
    (EMPTY_EID, None, 'whatever', 0),
    (ONLY_ONE, 'whatever', 'whatever', 0),
    (ONLY_ONE, None, None, 0),
    (ONLY_ONE, 'whatever', None, 0),
    (ONLY_ONE, None, 'whatever', 0),
    (ONLY_ONE, 'property', None, 1),
    (ONLY_ONE, None, 'property', 0),
    (ONLY_ONE_EID, 'whatever', 'whatever', 0),
    (ONLY_ONE_EID, None, None, 0),
    (ONLY_ONE_EID, 'whatever', None, 0),
    (ONLY_ONE_EID, None, 'whatever', 0),
    (ONLY_ONE_EID, 'attribute', None, 0),
    (ONLY_ONE_EID, None, 'attribute', 1),
    (ONLY_ONE_BOTH, 'whatever', 'whatever', 0),
    (ONLY_ONE_BOTH, None, None, 0),
    (ONLY_ONE_BOTH, 'whatever', None, 0),
    (ONLY_ONE_BOTH, None, 'whatever', 0),
    (ONLY_ONE_BOTH, 'property', 'attribute', 1),
    (ONLY_ONE_BOTH, 'property', None, 0),
    (ONLY_ONE_BOTH, None, 'attribute', 0),
    (COMBINASHUN, 'whatever', 'whatever', 0),
    (COMBINASHUN, None, None, 0),
    (COMBINASHUN, 'whatever', None, 0),
    (COMBINASHUN, None, 'whatever', 0),
    (COMBINASHUN, None, 'attribute', 1),
    (COMBINASHUN, None, 'attribute2', 1),
    (COMBINASHUN, 'property', 'attribute', 1),
    (COMBINASHUN, 'property', 'attribute2', 1),
    (COMBINASHUN, 'property', None, 1),
    (LOCALIZED, 'whatever', 'whatever', 0),
    (LOCALIZED, None, None, 0),
    (LOCALIZED, 'whatever', None, 0),
    (LOCALIZED, None, 'whatever', 0),
    (LOCALIZED, 'dc:title', None, 7),
    (LOCALIZED, 'dc:description', None, 5),
    (LOCALIZED, 'dc:title', 'whatever', 0),
    (LOCALIZED, 'dc:description', 'whatever', 0),
    (LOCALIZED, 'title', None, 0),
    (LOCALIZED, 'description', None, 0),
    (LOCALIZED, 'dc', None, 0),
    (LOCALIZED, ':', None, 0),
    ])
def test_getMetadata_by_pid_and_aid(item, pid, aid, expected):
    item = fromstring(item, XMLParser(target=Builder()))
    metadata = heimdall.getMetadata(item, pid=pid, aid=aid)
    assert len(metadata) is expected
    for m in metadata:
        assert m.get('pid') == pid
        assert m.text is not None
    # NOTE assert order of parameters doesn't matter:
    assert metadata == heimdall.getMetadata(item, aid=aid, pid=pid)


@pytest.mark.parametrize(('item', 'pid', 'aid', 'expected'), [
    (COMBI_NO_VALUE, 'whatever', 'whatever', 0),
    (COMBI_NO_VALUE, None, None, 0),
    (COMBI_NO_VALUE, 'whatever', None, 0),
    (COMBI_NO_VALUE, None, 'whatever', 0),
    (COMBI_NO_VALUE, None, 'attribute', 1),
    (COMBI_NO_VALUE, None, 'attribute2', 1),
    (COMBI_NO_VALUE, 'property', 'attribute', 1),
    (COMBI_NO_VALUE, 'property', 'attribute2', 1),
    (COMBI_NO_VALUE, 'property', None, 1),
    ])
def test_getMetadata_by_pid_and_aid_empty_value(item, pid, aid, expected):
    item = fromstring(item, XMLParser(target=Builder()))
    metadata = heimdall.getMetadata(item, pid=pid, aid=aid)
    assert len(metadata) is expected
    for m in metadata:
        assert m.get('pid') == pid
        assert m.text is None
    # NOTE assert order of parameters doesn't matter:
    assert metadata == heimdall.getMetadata(item, aid=aid, pid=pid)


@pytest.mark.parametrize(('item', 'lang', 'expected'), [
    (LOCALIZED, 'whatever', 0),
    (LOCALIZED, None, 2),
    (LOCALIZED, 'fr', 4),
    (LOCALIZED, 'en', 2),
    (LOCALIZED, 'ar', 1),
    (LOCALIZED, 'zh', 2),
    (LOCALIZED, 'emoji', 1),
    (EMPTY, 'whatever', 0),
    (EMPTY, None, 0),
    (EMPTY_EID, 'whatever', 0),
    (EMPTY_EID, None, 0),
    (ONLY_ONE, 'whatever', 0),
    (ONLY_ONE, None, 1),
    (ONLY_ONE_EID, 'whatever', 0),
    (ONLY_ONE_EID, None, 1),
    (ONLY_ONE_BOTH, 'whatever', 0),
    (ONLY_ONE_BOTH, None, 1),
    (COMBINASHUN, 'whatever', 0),
    (COMBINASHUN, None, 5),
    ])
def test_getMetadata_by_language(item, lang, expected):
    item = fromstring(item, XMLParser(target=Builder()))
    metadata = heimdall.getMetadata(item, lang=lang)
    assert len(metadata) is expected
    for m in metadata:
        assert heimdall.util.get_language(m) == lang
        assert m.text is not None


@pytest.mark.parametrize(('item', 'lang', 'pid', 'expected'), [
    (LOCALIZED, 'whatever', 'whatever', 0),
    (LOCALIZED, None, None, 0),
    (LOCALIZED, 'whatever', None, 0),
    (LOCALIZED, None, 'whatever', 0),
    (LOCALIZED, None, 'dc:title', 1),
    (LOCALIZED, 'fr', 'dc:title', 2),
    (LOCALIZED, 'en', 'dc:title', 1),
    (LOCALIZED, 'ar', 'dc:title', 1),
    (LOCALIZED, 'zh', 'dc:title', 2),
    (LOCALIZED, 'emoji', 'dc:title', 0),
    (LOCALIZED, None, 'dc:description', 1),
    (LOCALIZED, 'fr', 'dc:description', 2),
    (LOCALIZED, 'en', 'dc:description', 1),
    (LOCALIZED, 'ar', 'dc:description', 0),
    (LOCALIZED, 'zh', 'dc:description', 0),
    (LOCALIZED, 'emoji', 'dc:description', 1),
    (EMPTY, 'whatever', 'whatever', 0),
    (EMPTY, None, None, 0),
    (EMPTY, 'whatever', None, 0),
    (EMPTY, None, 'whatever', 0),
    (EMPTY_EID, 'whatever', 'whatever', 0),
    (EMPTY_EID, None, None, 0),
    (EMPTY_EID, 'whatever', None, 0),
    (EMPTY_EID, None, 'whatever', 0),
    (ONLY_ONE, 'whatever', 'whatever', 0),
    (ONLY_ONE, None, None, 0),
    (ONLY_ONE, 'whatever', None, 0),
    (ONLY_ONE, None, 'whatever', 0),
    (ONLY_ONE_EID, 'whatever', 'whatever', 0),
    (ONLY_ONE_EID, None, None, 1),
    (ONLY_ONE_EID, 'whatever', None, 0),
    (ONLY_ONE_EID, None, 'whatever', 0),
    (ONLY_ONE_BOTH, 'whatever', 'whatever', 0),
    (ONLY_ONE_BOTH, None, None, 0),
    (ONLY_ONE_BOTH, 'whatever', None, 0),
    (ONLY_ONE_BOTH, None, 'whatever', 0),
    (COMBINASHUN, 'whatever', 'whatever', 0),
    (COMBINASHUN, None, None, 2),
    (COMBINASHUN, 'whatever', None, 0),
    (COMBINASHUN, None, 'whatever', 0),
    ])
def test_getMetadata_by_language_and_pid(item, lang, pid, expected):
    item = fromstring(item, XMLParser(target=Builder()))
    metadata = heimdall.getMetadata(item, lang=lang, pid=pid)
    assert len(metadata) is expected
    for m in metadata:
        assert heimdall.util.get_language(m) == lang
        assert m.text is not None
    # NOTE assert order of parameters doesn't matter:
    assert metadata == heimdall.getMetadata(item, pid=pid, lang=lang)


@pytest.mark.parametrize(('item', 'lang', 'pid', 'expected'), [
    (LOCALIZED, 'whatever', 'whatever', 0),
    (LOCALIZED, None, None, 0),
    (LOCALIZED, 'whatever', None, 0),
    (LOCALIZED, None, 'whatever', 0),
    (LOCALIZED, None, 'dc:title', 1),
    (LOCALIZED, 'fr', 'dc:title', 2),
    (LOCALIZED, 'en', 'dc:title', 1),
    (LOCALIZED, 'ar', 'dc:title', 1),
    (LOCALIZED, 'zh', 'dc:title', 2),
    (LOCALIZED, 'emoji', 'dc:title', 0),
    (LOCALIZED, None, 'dc:description', 1),
    (LOCALIZED, 'fr', 'dc:description', 2),
    (LOCALIZED, 'en', 'dc:description', 1),
    (LOCALIZED, 'ar', 'dc:description', 0),
    (LOCALIZED, 'zh', 'dc:description', 0),
    (LOCALIZED, 'emoji', 'dc:description', 1),
    (EMPTY, 'whatever', 'whatever', 0),
    (EMPTY, None, None, 0),
    (EMPTY, 'whatever', None, 0),
    (EMPTY, None, 'whatever', 0),
    (EMPTY_EID, 'whatever', 'whatever', 0),
    (EMPTY_EID, None, None, 0),
    (EMPTY_EID, 'whatever', None, 0),
    (EMPTY_EID, None, 'whatever', 0),
    (ONLY_ONE, 'whatever', 'whatever', 0),
    (ONLY_ONE, None, None, 0),
    (ONLY_ONE, 'whatever', None, 0),
    (ONLY_ONE, None, 'whatever', 0),
    (ONLY_ONE_EID, 'whatever', 'whatever', 0),
    (ONLY_ONE_EID, None, None, 0),
    (ONLY_ONE_EID, 'whatever', None, 0),
    (ONLY_ONE_EID, None, 'whatever', 0),
    (ONLY_ONE_BOTH, 'whatever', 'whatever', 0),
    (ONLY_ONE_BOTH, None, None, 0),
    (ONLY_ONE_BOTH, 'whatever', None, 0),
    (ONLY_ONE_BOTH, None, 'whatever', 0),
    (COMBINASHUN, 'whatever', 'whatever', 0),
    (COMBINASHUN, None, None, 0),
    (COMBINASHUN, 'whatever', None, 0),
    (COMBINASHUN, None, 'whatever', 0),
    ])
def test_getMetadata_by_language_and_pid_no_aid(item, lang, pid, expected):
    item = fromstring(item, XMLParser(target=Builder()))
    metadata = heimdall.getMetadata(item, lang=lang, pid=pid, aid=None)
    assert len(metadata) is expected
    for m in metadata:
        assert heimdall.util.get_language(m) == lang
        assert m.text is not None
    # NOTE assert order of parameters doesn't matter:
    assert metadata == heimdall.getMetadata(item, pid=pid, aid=None, lang=lang)
    assert metadata == heimdall.getMetadata(item, lang=lang, aid=None, pid=pid)
    assert metadata == heimdall.getMetadata(item, aid=None, pid=pid, lang=lang)
    assert metadata == heimdall.getMetadata(item, aid=None, lang=lang, pid=pid)


@pytest.mark.parametrize(('item', 'param', 'value', 'expected'), [
    (EMPTY, 'whatever', 'whatever', 0),
    (EMPTY_EID, 'whatever', 'whatever', 0),
    (ONLY_ONE, 'whatever', 'whatever', 0),
    (ONLY_ONE_EID, 'whatever', 'whatever', 0),
    (COMBINASHUN, 'custom', 'value', 2),
    (COMBINASHUN, 'custom', None, 3),
    (LOCALIZED, 'floof', 'high', 2),
    (LOCALIZED, 'floof', None, 10),
    (LOCALIZED, 'floof', 'whatever', 0),
    ])
def test_getMetadata_by_custom_param(item, param, value, expected):
    item = fromstring(item, XMLParser(target=Builder()))
    metadata = heimdall.getMetadata(item, **{param: value, })
    assert len(metadata) is expected
    for m in metadata:
        assert m.text is not None


@pytest.mark.parametrize(('item', 'param', 'value', 'lang', 'expected'), [
    (EMPTY, 'whatever', 'whatever', 'whatever', 0),
    (EMPTY_EID, 'whatever', 'whatever', 'whatever', 0),
    (ONLY_ONE, 'whatever', 'whatever', 'whatever', 0),
    (ONLY_ONE_EID, 'whatever', 'whatever', 'whatever', 0),
    (COMBINASHUN, 'custom', 'value', None, 2),
    (COMBINASHUN, 'custom', 'value', 'whatever', 0),
    (COMBINASHUN, 'custom', None, None, 3),
    (COMBINASHUN, 'custom', None, 'whatever', 0),
    (LOCALIZED, 'floof', 'high', 'fr', 1),
    (LOCALIZED, 'floof', 'high', 'emoji', 1),
    (LOCALIZED, 'floof', None, 'fr', 3),
    (LOCALIZED, 'floof', None, 'emoji', 0),
    (LOCALIZED, 'floof', 'whatever', 'fr', 0),
    (LOCALIZED, 'floof', 'whatever', 'emoji', 0),
    ])
def test_getMetadata_by_custom_param(item, param, value, lang, expected):
    item = fromstring(item, XMLParser(target=Builder()))
    metadata = heimdall.getMetadata(item, **{param: value, 'lang': lang, })
    assert len(metadata) is expected
    for m in metadata:
        assert m.text is not None


@pytest.mark.parametrize(('item', 'expected'), [
    (EMPTY, 0),
    (EMPTY_EID, 0),
    (ONLY_ONE, 1),
    (ONLY_ONE_EID, 1),
    (COMBINASHUN, 5),
    (LOCALIZED, 12),
    ])
def test_getValues_no_option(item, expected):
    item = fromstring(item, XMLParser(target=Builder()))
    values = heimdall.getValues(item)
    assert len(values) is expected
    for value in values:
        assert value is not None and len(value) > 0


@pytest.mark.parametrize(('item', 'expected'), [
    (COMBI_NO_VALUE, 5),
    ])
def test_getValues_no_option_no_value(item, expected):
    item = fromstring(item, XMLParser(target=Builder()))
    values = heimdall.getValues(item)
    assert len(values) is expected
    for value in values:
        assert value is ''


@pytest.mark.parametrize(('item', 'expected'), [
    (EMPTY, 0),
    (EMPTY_EID, 0),
    (ONLY_ONE, 1),
    (ONLY_ONE_EID, 1),
    ])
def test_getValue_no_option(item, expected):
    item = fromstring(item, XMLParser(target=Builder()))
    value = heimdall.getValue(item)
    assert value is None or len(value) > 0


@pytest.mark.parametrize(('item', 'expected'), [
    (COMBINASHUN, 5),
    (COMBI_NO_VALUE, 5),
    (LOCALIZED, 12),
    ])
def test_getValue_no_option_IndexError(item, expected):
    item = fromstring(item, XMLParser(target=Builder()))
    try:
        heimdall.getValue(item)
        assert False
    except IndexError:
        assert True


@pytest.mark.parametrize(('item', 'options', 'before', 'after'), [
    (ONLY_ONE, {'aid': 'attribute', 'pid': 'property'}, 1, 1),
    (ONLY_ONE, {'aid': 'attribute'}, 1, 1),
    (ONLY_ONE, {'pid': 'property'}, 1, 0),
    (ONLY_ONE_EID, {'aid': 'attribute', 'pid': 'property'}, 1, 1),
    (ONLY_ONE_EID, {'aid': 'attribute'}, 1, 0),
    (ONLY_ONE_EID, {'pid': 'property'}, 1, 1),
    (ONLY_ONE_BOTH, {'aid': 'attribute', 'pid': 'property'}, 1, 0),
    (ONLY_ONE_BOTH, {'aid': 'attribute'}, 1, 0),
    (ONLY_ONE_BOTH, {'pid': 'property'}, 1, 0),
    ])
def test_deleteMetadata(item, options, before, after):
    item = fromstring(item, XMLParser(target=Builder()))
    assert len(heimdall.getMetadata(item)) == before
    heimdall.deleteMetadata(item, **options)
    assert len(heimdall.getMetadata(item)) == after


@pytest.mark.parametrize(('item', 'options'), [
    (EMPTY, {'aid': 'whatever', 'pid': 'whatever'}),
    (EMPTY, {'pid': 'whatever'}),
    (EMPTY, {'aid': 'whatever'}),
    (EMPTY_EID, {'aid': 'whatever', 'pid': 'whatever'}),
    (EMPTY_EID, {'pid': 'whatever'}),
    (EMPTY_EID, {'aid': 'whatever'}),
    (ONLY_ONE, {'aid': 'whatever', 'pid': 'whatever'}),
    (ONLY_ONE, {'pid': 'whatever'}),
    (ONLY_ONE, {'aid': 'whatever'}),
    (ONLY_ONE_EID, {'aid': 'whatever', 'pid': 'whatever'}),
    (ONLY_ONE_EID, {'pid': 'whatever'}),
    (ONLY_ONE_EID, {'aid': 'whatever'}),
    (ONLY_ONE_BOTH, {'aid': 'whatever', 'pid': 'whatever'}),
    (ONLY_ONE_BOTH, {'pid': 'whatever'}),
    (ONLY_ONE_BOTH, {'aid': 'whatever'}),
    (COMBINASHUN, {'aid': 'whatever', 'pid': 'whatever'}),
    (COMBINASHUN, {'pid': 'whatever'}),
    (COMBINASHUN, {'aid': 'whatever'}),
    (LOCALIZED, {'aid': 'whatever', 'pid': 'whatever'}),
    (LOCALIZED, {'aid': 'whatever'}),
    (LOCALIZED, {'pid': 'whatever'}),
    ])
def test_deleteMetadata_not_found(item, options):
    item = fromstring(item, XMLParser(target=Builder()))
    before = heimdall.getMetadata(item)
    heimdall.deleteMetadata(item, **options)
    after = heimdall.getMetadata(item)
    assert len(before) == len(after)


@pytest.mark.parametrize(('item', 'options'), [
    (EMPTY, {}),
    (EMPTY_EID, {}),
    (ONLY_ONE, {}),
    (ONLY_ONE_EID, {}),
    (ONLY_ONE_BOTH, {}),
    (COMBINASHUN, {}),
    (LOCALIZED, {}),
    ])
def test_deleteMetadata_all(item, options):
    item = fromstring(item, XMLParser(target=Builder()))
    heimdall.deleteMetadata(item, **options)
    assert len(heimdall.getMetadata(item)) == 0
