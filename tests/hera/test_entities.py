# -*- coding: utf-8 -*-
import pytest
import heimdall
from ..stubs import *
from heimdall.util import get_node, get_nodes


# ################
# # GET entities #
# ################

@pytest.mark.parametrize(('db', 'expected'), [
    (like_rilly_empty_db, 0),
    (as_good_as_empty_db, 0),
    (only_1_item_pea_db, 1),
    (mini_consistent_db, 2),
    ])
def test_getEntities(db, expected):
    results = heimdall.getEntities(db())
    assert results is not None
    assert len(results) == expected


@pytest.mark.parametrize(('db'), [
    (like_rilly_empty_db),
    (as_good_as_empty_db),
    (only_1_item_pea_db),
    (mini_consistent_db),
    ])
def test_getEntity_fails_if_no_filter(db):
    try:
        heimdall.getEntity(db())  # always raises TypeError, so ...
        assert False  # ... we should never reach this assert
    except TypeError:
        assert True


@pytest.mark.parametrize(('db'), [
    (like_rilly_empty_db),
    (as_good_as_empty_db),
    (only_1_item_pea_db),
    (mini_consistent_db),
    ])
def test_getEntity_no_result(db):
    result = heimdall.getEntity(db(), lambda x: False)
    assert result is None


@pytest.mark.parametrize(('db'), [
    (mini_consistent_db),
    ])
def test_getEntity_too_many_results(db):
    try:
        heimdall.getEntity(db(), lambda x: True)
        assert False  # we should never reach this
    except IndexError:
        assert True


@pytest.mark.parametrize(('db', 'eid', 'name', 'attrs_pid'), [
    (only_1_item_pea_db, 'entity', "Some entity idk", ['uid']),
    (mini_consistent_db, 'entity', "Some entity idk", ['uid', 'value']),
    ])
def test_getEntity_success(db, eid, name, attrs_pid):
    entity = heimdall.getEntity(db(), lambda e: e.get('id') == eid)
    assert entity is not None
    assert entity.get('id') == eid
    assert len(entity.name) == 1
    assert len(entity.name[None]) == 1
    assert name in entity.name[None]
    attributes = heimdall.getAttributes(entity)
    assert len(attributes) == len(attrs_pid)
    for a in attributes:
        assert a.get('pid') in attrs_pid


# ###################
# # CREATE entities #
# ###################

@pytest.mark.parametrize(('db'), [
    (like_rilly_empty_db),
    (as_good_as_empty_db),
    (only_1_item_pea_db),
    (mini_consistent_db),
    ])
def test_createEntity_no_kwargs_no_problem(db):
    tree = db()
    EID = 'test'
    size = len(heimdall.getEntities(tree))
    heimdall.createEntity(tree, EID)
    assert len(heimdall.getEntities(tree)) == size+1
    e = heimdall.getEntity(tree, lambda e: e.get('id') == EID)
    assert e is not None
    assert e.get('id') == EID
    assert get_node(e, 'uri') is None
    assert get_node(e, 'name') is None
    assert get_node(e, 'description') is None


@pytest.mark.parametrize(('db'), [
    (like_rilly_empty_db),
    (as_good_as_empty_db),
    (only_1_item_pea_db),
    (mini_consistent_db),
    ])
def test_createEntity_more_than_once(db):
    tree = db()
    size = len(heimdall.getEntities(tree))
    heimdall.createEntity(tree, 'x')
    heimdall.createEntity(tree, 'y')
    heimdall.createEntity(tree, 'z')
    assert len(heimdall.getEntities(tree)) == size+3


@pytest.mark.parametrize(('db'), [
    (like_rilly_empty_db),
    (as_good_as_empty_db),
    (only_1_item_pea_db),
    (mini_consistent_db),
    ])
def test_createEntity_cannot_use_same_id_twice(db):
    tree = db()
    size = len(heimdall.getEntities(tree))
    heimdall.createEntity(tree, 'test')
    try:
        heimdall.createEntity(tree, 'test')  # always raises error, so ...
        assert False  # ... we should never reach this assert
    except ValueError:
        assert True


@pytest.mark.parametrize(('db'), [
    (like_rilly_empty_db),
    (as_good_as_empty_db),
    (only_1_item_pea_db),
    (mini_consistent_db),
    ])
def test_createEntity_is_consistent(db):
    tree = db()
    size = len(heimdall.getEntities(tree))
    heimdall.createEntity(
        tree, 'test',
        uri=['http://example.com/entities/test'],
        name="Moo!",
        description="MOO I said"
        )
    assert len(heimdall.getEntities(tree)) == size+1
    e = heimdall.getEntity(tree, lambda e: e.get('id') == 'test')
    assert e is not None
    assert e.get('id') == 'test'
    assert get_node(e, 'uri').text == 'http://example.com/entities/test'
    assert get_node(e, 'name').text == "Moo!"
    assert get_node(e, 'description').text == "MOO I said"


@pytest.mark.parametrize(('db'), [
    (like_rilly_empty_db),
    (as_good_as_empty_db),
    (only_1_item_pea_db),
    (mini_consistent_db),
    ])
def test_createEntity_empty_uri_list_is_okay(db):
    tree = db()
    size = len(heimdall.getEntities(tree))
    heimdall.createEntity(
        tree, 'test',
        uri=[],
        )
    assert len(heimdall.getEntities(tree)) == size+1
    e = heimdall.getEntity(tree, lambda e: e.get('id') == 'test')
    assert e is not None
    assert e.get('id') == 'test'
    assert get_node(e, 'uri') is None


@pytest.mark.parametrize(('db'), [
    (like_rilly_empty_db),
    (as_good_as_empty_db),
    (only_1_item_pea_db),
    (mini_consistent_db),
    ])
def test_createEntity_uri_must_be_list(db):
    tree = db()
    try:
        heimdall.createEntity(
            tree, 'test',
            uri='http://example.com/entities/test',
            )  # always raises TypeError, so ...
        assert False  # ... we should never reach this assert
    except TypeError:
        assert True


@pytest.mark.parametrize(('db'), [
    (like_rilly_empty_db),
    (as_good_as_empty_db),
    (only_1_item_pea_db),
    (mini_consistent_db),
    ])
def test_createEntity_i18n(db):
    tree = db()
    size = len(heimdall.getEntities(tree))
    heimdall.createEntity(
        tree, 'test',
        name={'en': "Moo!", 'fr': "Meuh!", },
        description={'en': "MOO I said", 'fr': "J'ai dit MEUH", },
        )
    assert len(heimdall.getEntities(tree)) == size+1
    e = heimdall.getEntity(tree, lambda e: e.get('id') == 'test')
    assert e is not None
    assert e.get('id') == 'test'
    values = [node.text for node in get_nodes(e, 'uri')]
    assert len(values) == 0
    values = [node.text for node in get_nodes(e, 'name')]
    assert len(values) == 2
    assert "Moo!" in values
    assert "Meuh!" in values
    values = [node.text for node in get_nodes(e, 'description')]
    assert len(values) == 2
    assert "MOO I said" in values
    assert "J'ai dit MEUH" in values


@pytest.mark.parametrize(('db'), [
    (like_rilly_empty_db),
    (as_good_as_empty_db),
    (only_1_item_pea_db),
    (mini_consistent_db),
    ])
def test_createEntity_uri_is_not_i18n_like_this(db):
    tree = db()
    try:
        heimdall.createEntity(
            tree, 'test',
            uri=[{'en': 'https://example.com', 'fr': 'http://exemple.fr', },],
            )  # always raises typeerror, so ...
        assert False  # ... we should never reach this assert
    except TypeError:
        assert True


@pytest.mark.parametrize(('db'), [
    (like_rilly_empty_db),
    (as_good_as_empty_db),
    (only_1_item_pea_db),
    (mini_consistent_db),
    ])
def test_createEntity_uri_is_not_i18n_like_that_either(db):
    tree = db()
    try:
        heimdall.createEntity(
            tree, 'test',
            uri={'en': ['https://example.com'],
                 'fr': ['http://exemple.fr'],
                 },
            )  # always raises typeerror, so ...
        assert False  # ... we should never reach this assert
    except TypeError:
        assert True


# ###################
# # CREATE entities #
# ###################

@pytest.mark.parametrize(('db'), [
    (only_1_item_pea_db),
    (mini_consistent_db),
    ])
def test_updateEntity_id(db):
    entities = heimdall.getEntities(db())
    for entity in entities:
        assert entity.get('id') != 'test'
        heimdall.updateEntity(entity, id='test')
        assert entity.get('id') == 'test'


@pytest.mark.parametrize('db', [
    only_1_item_pea_db,
    mini_consistent_db,
    ])
@pytest.mark.parametrize('tag', [
    'name', 'description',
    ])
@pytest.mark.parametrize(('value', 'deleted'), [
    ('test', False),
    ('101010', False),
    ('حيوان أليف', False),
    ('宠儿', False),
    ('🐶🐱🐭🐹🐰🦜🐠🐢', False),
    (None, True),
    ])
def test_updateEntity_update_child(db, tag, value, deleted):
    entities = heimdall.getEntities(db())
    for entity in entities:
        before = get_node(entity, tag, direct=True)
        assert before is None or before.text != value
        heimdall.updateEntity(entity, **{tag: value})
        after = get_node(entity, tag, direct=True)
        assert deleted or (after is not None)
        assert deleted or after.text == value


@pytest.mark.parametrize('db', [
    only_1_item_pea_db,
    mini_consistent_db,
    ])
@pytest.mark.parametrize('attribute', [
    {'pid': 'uid', 'max': '1', 'name': 'test', },
    {'pid': 'value', 'name': 'test', 'description': 'TEST', },
    ])
def test_updateEntity_update_attribute(db, attribute):
    def by_pid(attr):
        # aid = attribute.get('id', None)
        pid = attribute.get('pid', None)
        return attr.get('pid') == pid

    entities = heimdall.getEntities(db())
    for entity in entities:
        heimdall.updateEntity(entity, attributes=[attribute, ])
        a = heimdall.getAttribute(entity, by_pid)
        for tag, value in attribute.items():
            if tag in ['id', 'pid', 'min', 'max', ]:
                assert a.get(tag) == value
            else:
                assert get_node(a, tag).text == value


# ###################
# # DELETE entities #
# ###################


@pytest.mark.parametrize(('db'), [
    (like_rilly_empty_db),
    (as_good_as_empty_db),
    (only_1_item_pea_db),
    (mini_consistent_db),
    ])
def test_deleteEntity_fails_if_no_filter(db):
    try:
        heimdall.deleteEntity(db())  # always raises TypeError, so ...
        assert False  # ... we should never reach this assert
    except TypeError:
        assert True


@pytest.mark.parametrize(('db', 'expected'), [
    (like_rilly_empty_db, 0),
    (as_good_as_empty_db, 0),
    (only_1_item_pea_db, 1),
    (mini_consistent_db, 2),
    ])
def test_deleteEntity_does_nothing_if_filter_does_nothing(db, expected):
    tree = db()
    assert len(heimdall.getEntities(tree)) == expected
    heimdall.deleteEntity(tree, lambda x: False)
    assert len(heimdall.getEntities(tree)) == expected


@pytest.mark.parametrize(('db'), [
    (like_rilly_empty_db),
    (as_good_as_empty_db),
    ])
def test_deleteItem_does_nothing_on_empty_db(db):
    tree = db()
    assert len(heimdall.getEntities(tree)) == 0
    heimdall.deleteEntity(tree, lambda x: True)
    assert len(heimdall.getEntities(tree)) == 0


@pytest.mark.parametrize(('db', 'before', 'after'), [
    (only_1_item_pea_db, 1, 0),
    (mini_consistent_db, 2, 1),
    ])
def test_deleteEntity_deletes_1_success(db, before, after):
    tree = db()
    assert len(heimdall.getEntities(tree)) == before

    def by_eid(entity):
        return entity.get('id') == 'entity'

    heimdall.deleteEntity(tree, by_eid)
    assert len(heimdall.getEntities(tree)) == after


@pytest.mark.parametrize(('db'), [
    (mini_consistent_db),
    ])
def test_deleteEntity_deletes_more_than_1_success(db):
    tree = db()
    assert len(heimdall.getEntities(tree)) >= 2
    heimdall.deleteEntity(tree, lambda x: True)
    assert len(heimdall.getEntities(tree)) == 0
