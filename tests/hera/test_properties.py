# -*- coding: utf-8 -*-
import pytest
import heimdall
from ..stubs import *
from heimdall.util import get_node, get_nodes


# ##################
# # GET properties #
# ##################

@pytest.mark.parametrize(('db', 'expected'), [
    (like_rilly_empty_db, 0),
    (as_good_as_empty_db, 0),
    (only_1_item_pea_db, 1),
    (mini_consistent_db, 3),
    ])
def test_getProperties(db, expected):
    results = heimdall.getProperties(db())
    assert results is not None
    assert len(results) == expected


@pytest.mark.parametrize(('db'), [
    (like_rilly_empty_db),
    (as_good_as_empty_db),
    (only_1_item_pea_db),
    (mini_consistent_db),
    ])
def test_getProperty_fails_if_no_filter(db):
    try:
        heimdall.getProperty(db())  # always raises TypeError, so ...
        assert False  # ... we should never reach this assert
    except TypeError:
        assert True


@pytest.mark.parametrize(('db'), [
    (like_rilly_empty_db),
    (as_good_as_empty_db),
    (only_1_item_pea_db),
    (mini_consistent_db),
    ])
def test_getProperty_no_result(db):
    result = heimdall.getProperty(db(), lambda x: False)
    assert result is None


@pytest.mark.parametrize(('db'), [
    (mini_consistent_db),
    ])
def test_getProperty_too_many_results(db):
    try:
        heimdall.getProperty(db(), lambda x: True)
        assert False  # we should never reach this
    except IndexError:
        assert True


@pytest.mark.parametrize(('db', 'pid', 'name'), [
    (only_1_item_pea_db, 'uid', "Unique identifier"),
    (mini_consistent_db, 'uid', "Unique identifier"),
    ])
def test_getProperty_success(db, pid, name):

    def by_pid(prop):
        return prop.get('id') == pid
    prop = heimdall.getProperty(db(), by_pid)
    assert prop is not None
    assert prop.get('id') == pid
    assert get_node(prop, 'name').text == name


# #####################
# # CREATE properties #
# #####################

@pytest.mark.parametrize(('db'), [
    (like_rilly_empty_db),
    (as_good_as_empty_db),
    (only_1_item_pea_db),
    (mini_consistent_db),
    ])
def test_createProperty_no_kwargs_no_problem(db):
    tree = db()
    PID = 'test'
    size = len(heimdall.getProperties(tree))
    heimdall.createProperty(tree, PID)
    assert len(heimdall.getProperties(tree)) == size+1
    p = heimdall.getProperty(tree, lambda p: p.get('id') == PID)
    assert p is not None
    assert p.get('id') == PID
    assert get_node(p, 'type').text == 'text'
    assert get_node(p, 'uri') is None
    assert get_node(p, 'name') is None
    assert get_node(p, 'description') is None


@pytest.mark.parametrize(('db'), [
    (like_rilly_empty_db),
    (as_good_as_empty_db),
    (only_1_item_pea_db),
    (mini_consistent_db),
    ])
def test_createProperty_more_than_once(db):
    tree = db()
    size = len(heimdall.getProperties(tree))
    heimdall.createProperty(tree, 'x')
    heimdall.createProperty(tree, 'y')
    heimdall.createProperty(tree, 'z')
    assert len(heimdall.getProperties(tree)) == size+3


@pytest.mark.parametrize(('db'), [
    (like_rilly_empty_db),
    (as_good_as_empty_db),
    (only_1_item_pea_db),
    (mini_consistent_db),
    ])
def test_createProperty_cannot_use_same_pid_twice(db):
    tree = db()
    size = len(heimdall.getProperties(tree))
    heimdall.createProperty(tree, 'test')
    try:
        heimdall.createProperty(tree, 'test')  # always raises error, so ...
        assert False  # ... we should never reach this assert
    except ValueError:
        assert True


@pytest.mark.parametrize(('db'), [
    (like_rilly_empty_db),
    (as_good_as_empty_db),
    (only_1_item_pea_db),
    (mini_consistent_db),
    ])
def test_createProperty_is_consistent(db):
    tree = db()
    size = len(heimdall.getProperties(tree))
    heimdall.createProperty(
        tree, 'test',
        type='whatever',
        uri=['http://example.com/properties/test'],
        name="Moo!",
        description="MOO I said"
        )
    assert len(heimdall.getProperties(tree)) == size+1
    p = heimdall.getProperty(tree, lambda p: p.get('id') == 'test')
    assert p is not None
    assert p.get('id') == 'test'
    assert get_node(p, 'type').text == 'whatever'
    assert get_node(p, 'uri').text == 'http://example.com/properties/test'
    assert get_node(p, 'name').text == "Moo!"
    assert get_node(p, 'description').text == "MOO I said"


@pytest.mark.parametrize(('db'), [
    (like_rilly_empty_db),
    (as_good_as_empty_db),
    (only_1_item_pea_db),
    (mini_consistent_db),
    ])
def test_createProperty_empty_uri_list_is_okay(db):
    tree = db()
    size = len(heimdall.getProperties(tree))
    heimdall.createProperty(
        tree, 'test',
        uri=[],
        )
    assert len(heimdall.getProperties(tree)) == size+1
    p = heimdall.getProperty(tree, lambda p: p.get('id') == 'test')
    assert p is not None
    assert p.get('id') == 'test'
    assert get_node(p, 'uri') is None


@pytest.mark.parametrize(('db'), [
    (like_rilly_empty_db),
    (as_good_as_empty_db),
    (only_1_item_pea_db),
    (mini_consistent_db),
    ])
def test_createProperty_uri_must_be_list(db):
    tree = db()
    try:
        heimdall.createProperty(
            tree, 'test',
            uri='http://example.com/properties/test',
            )  # always raises TypeError, so ...
        assert False  # ... we should never reach this assert
    except TypeError:
        assert True


@pytest.mark.parametrize(('db'), [
    (like_rilly_empty_db),
    (as_good_as_empty_db),
    (only_1_item_pea_db),
    (mini_consistent_db),
    ])
def test_createProperty_i18n(db):
    tree = db()
    size = len(heimdall.getProperties(tree))
    heimdall.createProperty(
        tree, 'test',
        name={'en': "Moo!", 'fr': "Meuh!", },
        description={'en': "MOO I said", 'fr': "J'ai dit MEUH", },
        )
    assert len(heimdall.getProperties(tree)) == size+1
    p = heimdall.getProperty(tree, lambda p: p.get('id') == 'test')
    assert p is not None
    assert p.get('id') == 'test'
    values = [node.text for node in get_nodes(p, 'uri')]
    assert len(values) == 0
    values = [node.text for node in get_nodes(p, 'name')]
    assert len(values) == 2
    assert "Moo!" in values
    assert "Meuh!" in values
    values = [node.text for node in get_nodes(p, 'description')]
    assert len(values) == 2
    assert "MOO I said" in values
    assert "J'ai dit MEUH" in values


@pytest.mark.parametrize(('db'), [
    (like_rilly_empty_db),
    (as_good_as_empty_db),
    (only_1_item_pea_db),
    (mini_consistent_db),
    ])
def test_createProperty_type_is_not_i18n(db):
    tree = db()
    try:
        heimdall.createProperty(
            tree, 'test',
            type={'en': 'text', 'fr': 'texte', },
            )  # always raises typeerror, so ...
        assert False  # ... we should never reach this assert
    except TypeError:
        assert True


@pytest.mark.parametrize(('db'), [
    (like_rilly_empty_db),
    (as_good_as_empty_db),
    (only_1_item_pea_db),
    (mini_consistent_db),
    ])
def test_createProperty_uri_is_not_i18n_like_this(db):
    tree = db()
    try:
        heimdall.createProperty(
            tree, 'test',
            uri=[{'en': 'https://example.com', 'fr': 'http://exemple.fr', },],
            )  # always raises typeerror, so ...
        assert False  # ... we should never reach this assert
    except TypeError:
        assert True


@pytest.mark.parametrize(('db'), [
    (like_rilly_empty_db),
    (as_good_as_empty_db),
    (only_1_item_pea_db),
    (mini_consistent_db),
    ])
def test_createProperty_uri_is_not_i18n_like_that_either(db):
    tree = db()
    try:
        heimdall.createProperty(
            tree, 'test',
            uri={'en': ['https://example.com'],
                 'fr': ['http://exemple.fr'],
                 },
            )  # always raises typeerror, so ...
        assert False  # ... we should never reach this assert
    except TypeError:
        assert True


# #####################
# # UPDATE properties #
# #####################

@pytest.mark.parametrize(('db', 'pid', 'before', 'kwargs', 'after'), [
    (only_1_item_pea_db, 'uid',
     {'id': 'uid', 'name': {None: ["Unique identifier"]}, },
     {'name': "Another message"},
     {'id': 'uid', 'name': {None: ["Another message"]}, },
     ),
    (only_1_item_pea_db, 'uid',
     {'id': 'uid', 'name': {None: ["Unique identifier"]}, },
     {'name': {'en': "Another", 'fr': "Un autre", }},
     {'id': 'uid', 'name': {'en': ["Another"], 'fr': ["Un autre"], }},
     ),
    (only_1_item_pea_db, 'uid',
     {'id': 'uid', 'name': {None: ["Unique identifier"]}, },
     {'name': {'en': "Another", 'fr': ["foo", "bar baz"], }},
     {'id': 'uid', 'name': {'en': ["Another"], 'fr': ["foo", "bar baz"], }},
     ),
    (mini_consistent_db, 'uid',
     {'id': 'uid', 'name': {None: ["Unique identifier"]}, },
     {'name': "Another message"},
     {'id': 'uid', 'name': {None: ["Another message"]}, },
     ),
    (mini_consistent_db, 'uid',
     {'id': 'uid', 'name': {None: ["Unique identifier"]}, },
     {'name': {'en': "Another", 'fr': "Un autre", }},
     {'id': 'uid', 'name': {'en': ["Another"], 'fr': ["Un autre"], }},
     ),
    (mini_consistent_db, 'uid',
     {'id': 'uid', 'name': {None: ["Unique identifier"]}, },
     {'name': {'en': "Another", 'fr': ["foo", "bar baz"], }},
     {'id': 'uid', 'name': {'en': ["Another"], 'fr': ["foo", "bar baz"], }},
     ),
    (mini_consistent_db, 'uid',
     {'id': 'uid', 'name': {None: ["Unique identifier"]}, },
     {'id': 'test'},
     {'id': 'test', 'name': {None: ["Unique identifier"]}, },
     ),
    (mini_consistent_db, 'uid',
     {'id': 'uid', 'name': {None: ["Unique identifier"]}, },
     {'id': None},  # doing this would be MEH, but in test context... it works
     {'id': None, 'name': {None: ["Unique identifier"]}, },
     ),
    (mini_consistent_db, 'uid',
     {'id': 'uid', 'name': {None: ["Unique identifier"]}, },
     {'id': 'test', 'name': "Another message"},
     {'id': 'test', 'name': {None: ["Another message"]}, },
     ),
    (mini_consistent_db, 'uid',
     {'id': 'uid', 'name': {None: ["Unique identifier"]}, },
     {'name': {'en': "Another", 'fr': "Un autre", }, 'id': 'test'},
     {'id': 'test', 'name': {'en': ["Another"], 'fr': ["Un autre"], }},
     ),
    (mini_consistent_db, 'uid',
     {'id': 'uid', 'name': {None: ["Unique identifier"]}, },
     {'name': {'en': "Another", 'fr': ["foo", "bar baz"], }, 'id': 'test'},
     {'name': {'en': ["Another"], 'fr': ["foo", "bar baz"], }, 'id': 'test'},
     ),
    ])
def test_updateProperty(db, pid, before, kwargs, after):
    def _assert(p, expected):
        for k, v in expected.items():
            if k in ['id', ]:
                assert p.get(k) == v
            else:
                assert getattr(p, k) == v

    p = heimdall.getProperty(db(), lambda x: x.get('id') == pid)
    _assert(p, before)
    heimdall.updateProperty(p, **kwargs)
    _assert(p, after)


# #####################
# # DELETE properties #
# #####################

@pytest.mark.parametrize(('db'), [
    (like_rilly_empty_db),
    (as_good_as_empty_db),
    (only_1_item_pea_db),
    (mini_consistent_db),
    ])
def test_deleteProperty_fails_if_no_filter(db):
    try:
        heimdall.deleteProperty(db())  # always raises TypeError, so ...
        assert False  # ... we should never reach this assert
    except TypeError:
        assert True


@pytest.mark.parametrize(('db', 'expected'), [
    (like_rilly_empty_db, 0),
    (as_good_as_empty_db, 0),
    (only_1_item_pea_db, 1),
    (mini_consistent_db, 3),
    ])
def test_deleteProperty_does_nothing_if_filter_does_nothing(db, expected):
    tree = db()
    assert len(heimdall.getProperties(tree)) == expected
    heimdall.deleteProperty(tree, lambda x: False)
    assert len(heimdall.getProperties(tree)) == expected


@pytest.mark.parametrize(('db'), [
    (like_rilly_empty_db),
    (as_good_as_empty_db),
    ])
def test_deleteItem_does_nothing_on_empty_db(db):
    tree = db()
    assert len(heimdall.getProperties(tree)) == 0
    heimdall.deleteProperty(tree, lambda x: True)
    assert len(heimdall.getProperties(tree)) == 0


@pytest.mark.parametrize(('db', 'before', 'after'), [
    (only_1_item_pea_db, 1, 0),
    (mini_consistent_db, 3, 2),
    ])
def test_deleteProperty_deletes_1_success(db, before, after):
    tree = db()
    assert len(heimdall.getProperties(tree)) == before

    def by_pid(prop):
        return prop.get('id') == 'uid'

    heimdall.deleteProperty(tree, by_pid)
    assert len(heimdall.getProperties(tree)) == after


@pytest.mark.parametrize(('db'), [
    (mini_consistent_db),
    ])
def test_deleteProperty_deletes_more_than_1_success(db):
    tree = db()
    assert len(heimdall.getProperties(tree)) >= 2
    heimdall.deleteProperty(tree, lambda x: True)
    assert len(heimdall.getProperties(tree)) == 0
