# -*- coding: utf-8 -*-
import pytest
import heimdall
from ..stubs import *
from heimdall.util import get_node, get_nodes


# ##################
# # GET attributes #
# ##################

@pytest.mark.parametrize(('db', 'eid', 'expected'), [
    (only_1_item_pea_db, 'entity', 1),
    (mini_consistent_db, 'entity', 2),
    ])
def test_getAttributes(db, eid, expected):
    e = heimdall.getEntity(db(), lambda e: e.get('id') == eid)
    assert e is not None
    results = heimdall.getAttributes(e)
    assert results is not None
    assert len(results) == expected


@pytest.mark.parametrize(('db', 'eid'), [
    (only_1_item_pea_db, 'entity'),
    (mini_consistent_db, 'entity'),
    ])
def test_getAttribute_fails_if_no_filter(db, eid):
    e = heimdall.getEntity(db(), lambda e: e.get('id') == eid)
    assert e is not None
    try:
        heimdall.getAttribute(e)  # always raises TypeError, so ...
        assert False  # ... we should never reach this assert
    except TypeError:
        assert True


@pytest.mark.parametrize(('db', 'eid'), [
    (only_1_item_pea_db, 'entity'),
    (mini_consistent_db, 'entity'),
    ])
def test_getAttribute_no_result(db, eid):
    e = heimdall.getEntity(db(), lambda e: e.get('id') == eid)
    assert e is not None
    result = heimdall.getAttribute(e, lambda x: False)
    assert result is None


@pytest.mark.parametrize(('db', 'eid'), [
    (mini_consistent_db, 'entity'),
    ])
def test_getAttribute_too_many_results(db, eid):
    e = heimdall.getEntity(db(), lambda e: e.get('id') == eid)
    assert e is not None
    try:
        heimdall.getAttribute(e, lambda x: True)
        assert False  # we should never reach this
    except IndexError:
        assert True


@pytest.mark.parametrize(('db', 'eid', 'pid', 'name'), [
    (only_1_item_pea_db, 'entity', 'uid', None),
    (mini_consistent_db, 'entity', 'uid', "Unique identifier"),
    (mini_consistent_db, 'entity', 'value', None),
    ])
def test_getAttribute_success(db, eid, pid, name):
    e = heimdall.getEntity(db(), lambda e: e.get('id') == eid)
    assert e is not None
    a = heimdall.getAttribute(e, lambda a: a.get('pid') == pid)
    assert a is not None
    assert a.get('pid') == pid
    assert (((name is not None) and (get_node(a, 'name').text == name)) or
            ((name is None) and (get_node(a, 'name') is None)))


# #####################
# # CREATE attributes #
# #####################

@pytest.mark.parametrize(('db', 'eid'), [
    (only_1_item_pea_db, 'entity'),
    (mini_consistent_db, 'entity'),
    (mini_consistent_db, 'empty'),
    ])
def test_createAttribute_no_kwargs_no_problem(db, eid):
    e = heimdall.getEntity(db(), lambda e: e.get('id') == eid)
    assert e is not None
    size = len(heimdall.getAttributes(e))
    heimdall.createAttribute(e, pid='test')
    assert len(heimdall.getAttributes(e)) == size+1
    a = heimdall.getAttribute(e, lambda a: a.get('pid') == 'test')
    assert a is not None
    assert a.get('id') is None
    assert a.get('pid') == 'test'
    assert a.min is 0
    assert a.max is None
    assert get_node(a, 'uri') is None
    assert get_node(a, 'name') is None
    assert get_node(a, 'description') is None


@pytest.mark.parametrize(('db', 'eid'), [
    (only_1_item_pea_db, 'entity'),
    (mini_consistent_db, 'entity'),
    (mini_consistent_db, 'empty'),
    ])
def test_createAttribute_more_than_once(db, eid):
    e = heimdall.getEntity(db(), lambda e: e.get('id') == eid)
    assert e is not None
    size = len(heimdall.getAttributes(e))
    heimdall.createAttribute(e, pid='x')
    heimdall.createAttribute(e, pid='y')
    heimdall.createAttribute(e, pid='z')
    assert len(heimdall.getAttributes(e)) == size+3


@pytest.mark.parametrize(('db', 'eid'), [
    (only_1_item_pea_db, 'entity'),
    (mini_consistent_db, 'entity'),
    (mini_consistent_db, 'empty'),
    ])
def test_createAttribute_cannot_use_same_aid_twice(db, eid):
    e = heimdall.getEntity(db(), lambda e: e.get('id') == eid)
    assert e is not None
    size = len(heimdall.getAttributes(e))
    heimdall.createAttribute(e, id='test', pid='test')
    try:
        heimdall.createAttribute(e, id='test', pid='test')
        assert False
    except ValueError:
        assert True


@pytest.mark.parametrize(('db', 'eid'), [
    (only_1_item_pea_db, 'entity'),
    (mini_consistent_db, 'entity'),
    (mini_consistent_db, 'empty'),
    ])
def test_createAttribute_can_use_same_pid_twice_if_aid_are_different(db, eid):
    e = heimdall.getEntity(db(), lambda e: e.get('id') == eid)
    assert e is not None
    size = len(heimdall.getAttributes(e))
    heimdall.createAttribute(e, id='test1', pid='test')
    heimdall.createAttribute(e, id='test2', pid='test')
    assert len(heimdall.getAttributes(e)) == size+2


@pytest.mark.parametrize(('db', 'eid'), [
    (only_1_item_pea_db, 'entity'),
    (mini_consistent_db, 'entity'),
    (mini_consistent_db, 'empty'),
    ])
def test_createAttribute_is_consistent(db, eid):
    e = heimdall.getEntity(db(), lambda e: e.get('id') == eid)
    assert e is not None
    size = len(heimdall.getAttributes(e))
    heimdall.createAttribute(
        e, 'test', id='test_attr',
        min=1, max=3,
        type='whatever',
        uri=['http://example.com/attributes/test'],
        name="Moo!",
        description="MOO I said"
        )
    assert len(heimdall.getAttributes(e)) == size+1
    a = heimdall.getAttribute(e, lambda a: a.get('pid') == 'test')
    assert a is not None
    assert a.get('id') == 'test_attr'
    assert a.get('pid') == 'test'
    assert a.min == 1
    assert a.max == 3
    assert a.type == 'whatever'
    assert len(a.uri) == 1
    assert 'http://example.com/attributes/test' in a.uri
    assert len(a.name) == 1
    assert len(a.name[None]) == 1
    assert "Moo!" in a.name[None]
    assert len(a.description) == 1
    assert len(a.description[None]) == 1
    assert "MOO I said" in a.description[None]


@pytest.mark.parametrize(('db', 'eid'), [
    (only_1_item_pea_db, 'entity'),
    (mini_consistent_db, 'entity'),
    (mini_consistent_db, 'empty'),
    ])
def test_createAttribute_empty_uri_list_is_okay(db, eid):
    e = heimdall.getEntity(db(), lambda e: e.get('id') == eid)
    assert e is not None
    size = len(heimdall.getAttributes(e))
    heimdall.createAttribute(e, 'test', uri=[])
    assert len(heimdall.getAttributes(e)) == size+1
    a = heimdall.getAttribute(e, lambda a: a.get('pid') == 'test')
    assert a is not None
    assert a.get('pid') == 'test'
    assert get_node(a, 'uri') is None


@pytest.mark.parametrize(('db', 'eid'), [
    (only_1_item_pea_db, 'entity'),
    (mini_consistent_db, 'entity'),
    (mini_consistent_db, 'empty'),
    ])
def test_createAttribute_uri_must_be_list(db, eid):
    e = heimdall.getEntity(db(), lambda e: e.get('id') == eid)
    assert e is not None
    try:
        heimdall.createAttribute(
            e, 'test',
            uri='http://example.com/attributes/test',
            )  # always raises TypeError, so ...
        assert False  # ... we should never reach this assert
    except TypeError:
        assert True


@pytest.mark.parametrize(('db', 'eid'), [
    (only_1_item_pea_db, 'entity'),
    (mini_consistent_db, 'entity'),
    (mini_consistent_db, 'empty'),
    ])
def test_createAttribute_i18n(db, eid):
    e = heimdall.getEntity(db(), lambda e: e.get('id') == eid)
    assert e is not None
    size = len(heimdall.getAttributes(e))
    heimdall.createAttribute(
        e, 'test',
        name={'en': "Moo!", 'fr': "Meuh!", },
        description={'en': "MOO I said", 'fr': "J'ai dit MEUH", },
        )
    assert len(heimdall.getAttributes(e)) == size+1
    a = heimdall.getAttribute(e, lambda a: a.get('pid') == 'test')
    assert a is not None
    assert a.get('id') is None
    assert a.get('pid') == 'test'
    values = [node.text for node in get_nodes(a, 'uri')]
    assert len(values) == 0
    values = [node.text for node in get_nodes(a, 'name')]
    assert len(values) == 2
    assert "Moo!" in values
    assert "Meuh!" in values
    values = [node.text for node in get_nodes(a, 'description')]
    assert len(values) == 2
    assert "MOO I said" in values
    assert "J'ai dit MEUH" in values


@pytest.mark.parametrize(('db', 'eid'), [
    (only_1_item_pea_db, 'entity'),
    (mini_consistent_db, 'entity'),
    (mini_consistent_db, 'empty'),
    ])
def test_createAttribute_type_is_not_i18n(db, eid):
    e = heimdall.getEntity(db(), lambda e: e.get('id') == eid)
    try:
        heimdall.createAttribute(
            e, 'test',
            type={'en': 'text', 'fr': 'texte', },
            )  # always raises typeerror, so ...
        assert False  # ... we should never reach this assert
    except TypeError:
        assert True


@pytest.mark.parametrize(('db', 'eid'), [
    (only_1_item_pea_db, 'entity'),
    (mini_consistent_db, 'entity'),
    (mini_consistent_db, 'empty'),
    ])
def test_createAttribute_uri_is_not_i18n_like_this(db, eid):
    e = heimdall.getEntity(db(), lambda e: e.get('id') == eid)
    try:
        heimdall.createAttribute(
            e, 'test',
            uri=[{'en': 'https://example.com', 'fr': 'http://exemple.fr', },],
            )  # always raises typeerror, so ...
        assert False  # ... we should never reach this assert
    except TypeError:
        assert True


@pytest.mark.parametrize(('db', 'eid'), [
    (only_1_item_pea_db, 'entity'),
    (mini_consistent_db, 'entity'),
    (mini_consistent_db, 'empty'),
    ])
def test_createAttribute_uri_is_not_i18n_like_that_either(db, eid):
    e = heimdall.getEntity(db(), lambda e: e.get('id') == eid)
    try:
        heimdall.createAttribute(
            e, 'test',
            uri={'en': ['https://example.com'],
                 'fr': ['http://exemple.fr'],
                 },
            )  # always raises typeerror, so ...
        assert False  # ... we should never reach this assert
    except TypeError:
        assert True


# #####################
# # DELETE attributes #
# #####################


@pytest.mark.parametrize(('db', 'eid'), [
    (only_1_item_pea_db, 'entity'),
    (mini_consistent_db, 'entity'),
    ])
def test_deleteAttribute_fails_if_no_filter(db, eid):
    e = heimdall.getEntity(db(), lambda e: e.get('id') == eid)
    try:
        heimdall.deleteAttribute(e)  # always raises TypeError, so ...
        assert False  # ... we should never reach this assert
    except TypeError:
        assert True


@pytest.mark.parametrize(('db', 'eid', 'size'), [
    (only_1_item_pea_db, 'entity', 1),
    (mini_consistent_db, 'entity', 2),
    ])
def test_deleteAttribute_does_nothing_if_filter_does_nothing(db, eid, size):
    e = heimdall.getEntity(db(), lambda e: e.get('id') == eid)
    assert len(heimdall.getAttributes(e)) == size
    heimdall.deleteAttribute(e, lambda x: False)
    assert len(heimdall.getAttributes(e)) == size


@pytest.mark.parametrize(('db', 'eid'), [
    (mini_consistent_db, 'empty'),
    ])
def test_deleteItem_does_nothing_on_empty_entity(db, eid):
    e = heimdall.getEntity(db(), lambda e: e.get('id') == eid)
    assert len(heimdall.getAttributes(e)) == 0
    heimdall.deleteAttribute(e, lambda x: True)
    assert len(heimdall.getAttributes(e)) == 0


@pytest.mark.parametrize(('db', 'eid', 'pid', 'before', 'after'), [
    (only_1_item_pea_db, 'entity', 'uid', 1, 0),
    (mini_consistent_db, 'entity', 'uid', 2, 1),
    (mini_consistent_db, 'entity', 'value', 2, 1),
    ])
def test_deleteAttribute_success(db, eid, pid, before, after):
    e = heimdall.getEntity(db(), lambda e: e.get('id') == eid)
    assert len(heimdall.getAttributes(e)) == before
    heimdall.deleteAttribute(e, lambda a: a.get('pid') == pid)
    assert len(heimdall.getAttributes(e)) == after


@pytest.mark.parametrize(('db', 'eid'), [
    (mini_consistent_db, 'entity'),
    ])
def test_deleteAttribute_delete_more_than_1_success(db, eid):
    e = heimdall.getEntity(db(), lambda e: e.get('id') == eid)
    assert len(heimdall.getAttributes(e)) >= 2
    heimdall.deleteAttribute(e, lambda x: True)
    assert len(heimdall.getAttributes(e)) == 0
